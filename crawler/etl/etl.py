#TODO: fix encoding issues
#osiris@osiris-desktop:~/projects/snapvid/crawler_src$ (cd etl;python ./etl.py)
#Video Items count:1747
#./etl.py:26: Warning: Data truncated for column 'title' at row 1
#  cursor.execute(sql)
#./etl.py:26: Warning: Data truncated for column 'desc' at row 1
#  cursor.execute(sql)
#Skipped 604 due to errors
#Inserted 1143 videos successfully!

import csv
import MySQLdb
import traceback

conn = MySQLdb.connect(host="localhost",
                           user="root",
                           passwd="paXess7",
                           db="vse", port=2134)
dr = csv.DictReader(open('../results/items.csv'), delimiter=',',
                    fieldnames=['title', 'desc', 'url',
                                'pageUrl', 'thumbnail', 'externalID'])
cursor = conn.cursor()
video_items = []
for v in dr: #TODO: handle unicode properly
    title = str(v['title']) #,"utf-8",errors='ignore')
    desc = str(v['desc']) #,"utf-8",errors='ignore')
    url = v['url']
    pageUrl = v['pageUrl'].encode("ascii", "ignore")
    thumbnail = v['thumbnail'].encode("ascii", "ignore")
    externalID = v['externalID'].encode("ascii", "ignore")
    video_items.append((title, desc, url, pageUrl, thumbnail, externalID))

print "Video Items count:" + str(len(video_items))
skipped = 0
inserted = 0
for video in video_items:
    sql = "insert into search_video\
     (title, `desc`, url, pageUrl, thumbnail, externalID)\
      values (%s, %s, %s, %s, %s, %s)"
    try:
        cursor.execute(sql, video)
        inserted += 1
    except:
        skipped += 1
#        print "Error with:" + str(video)
        print "Error: " + traceback.format_exc()
        pass
conn.commit()
conn.close()

if skipped == len(video_items):
    print "Error: All were skipped due to errors"
else:
    print "Skipped " + str(skipped) + " due to errors"
print "Inserted " + str(inserted) + " videos successfully!"
