# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html

import csv

class CsvWriterPipeline(object):

    def __init__(self):
        self.csvwriter = csv.writer(open('results/items.csv', 'wb'))

    def process_item(self, spider, v):
        self.csvwriter.writerow([v['title'].encode('utf-8'), v['desc'].encode('utf-8'), v['url'], v['pageUrl'], v['thumbnail'], spider.domain_name + '_' + v['externalID']])
        return v
