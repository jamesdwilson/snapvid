# Scrapy settings for crawler_src project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#
# Or you can copy and paste them from where they're defined in Scrapy:
# 
#     scrapy/conf/default_settings.py
#

BOT_NAME = 'Bot by Dambach-Wilson james at jameswilson.name'
BOT_VERSION = '0.1'

SPIDER_MODULES = ['spiders']
NEWSPIDER_MODULE = 'spiders'
DEFAULT_ITEM_CLASS = 'items.Video'
USER_AGENT = '%s/%s' % (BOT_NAME, BOT_VERSION)

ITEM_PIPELINES = ['pipelines.CsvWriterPipeline']
ROBOTSTXT_OBEY = True