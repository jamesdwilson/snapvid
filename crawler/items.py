# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class Video(Item):
    title = Field()
    desc = Field()
#    cat = Field()
    tags = Field()
    pageUrl = Field()
    url = Field()
    duration = Field()
    rating = Field()
    thumbnail = Field()
    externalID = Field()
    