from scrapy.selector import HtmlXPathSelector
from items import Video
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor

class KillsometimeSpider(CrawlSpider):
    name = 'killsometime.com'
    domain_name = 'killsometime.com'
    allowed_domains = ['killsometime.com']
    start_urls = ['http://www.killsometime.com/']
    
    rules = (
             #Rule(SgmlLinkExtractor(allow=(r'/videos/date/[0-9]*?))),
             Rule(SgmlLinkExtractor(allow=(r'/videos/[0-9]*?/*/')),
                                    callback='parse_item',
                                    follow=True),
             )
    
    def parse_item(self, response):
        hxs = HtmlXPathSelector(response)
        
        v = Video()
        v['pageUrl'] = response.url
        v['title'] = hxs.select('/html/head/meta[@property="og:title"]/@content').extract()[0]
        v['url'] = hxs.select('/html/head/meta[@property="og:url"]/@content').extract()[0]
        v['desc'] = hxs.select('/html/head/meta[@name="description"]/@content').extract()[0].strip()
        v['thumbnail'] = hxs.select('/html/head/meta[@property="og:image"]/@content').extract()[0]
        v['tags'] = None
        v['duration'] = None
        #regex is a work in progress
        v['externalID'] = hxs.select('/html/head/meta[@property="og:url"]/@content').re(r'/videos/([0-9]*?)/')[0]
        return v
    
SPIDER = KillsometimeSpider()
        