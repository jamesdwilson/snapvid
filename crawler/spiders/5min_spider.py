from scrapy.selector import HtmlXPathSelector
from items import Video
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor


class FiveMinSpider(CrawlSpider):
    name = '5min.com'
    domain_name = '5min.com'
    allowed_domains = ['5min.com']
    start_urls = ['http://www.5min.com/']

    rules = (#Rule(SgmlLinkExtractor(allow=(r'http://www.break.com/[0-9]*$'))),
             Rule(SgmlLinkExtractor(allow=(r'5min.com/Video/'),
                                    deny=(r'#video-not-found')),
                                    callback='parse_item',
                                    follow=True),
            )

    def parse_item(self, response):
        hxs = HtmlXPathSelector(response)
        v = Video()
        v['pageUrl'] = response.url
        v['title'] = hxs.select('/html/head/\
        meta[@name="title"]/@content').extract()[0]
        v['url'] = hxs.select('/html/head/\
        link[@rel="video_src"]/@href').extract()[0]
        v['desc'] = hxs.select('/html/head/\
        meta[@name="description"]/@content').extract()[0].strip()
        v['thumbnail'] = hxs.select('/html/head/\
        link[@rel="videothumbnail"]/@href').extract()[0]
        v['tags'] = None
        v['duration'] = None
        v['externalID'] = hxs.select('/html/head/\
        link[@rel="video_src"]/@href').re(r'5min.com/([0-9]*?)/')[0]
        return v
SPIDER = FiveMinSpider()
