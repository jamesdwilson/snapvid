from scrapy.selector import HtmlXPathSelector
from items import Video
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor

class CHSpider(CrawlSpider):
    name = 'collegehumor.com'
    domain_name = 'collegehumor.com'
    allowed_domains = ['collegehumor.com']
    start_urls = ['http://www.collegehumor.com/videos/page:1']

    rules = (Rule(SgmlLinkExtractor(allow=(r'videos/page:[0-9]*$'))),
             Rule(SgmlLinkExtractor(allow=(r'/video:[0-9]*$')),
                                    callback='parse_item'),
            )

    def parse_item(self, response):
        hxs = HtmlXPathSelector(response)
        
        v = Video()
        v['pageUrl'] = response.url
        v['title'] = hxs.select('/html/head/meta[@name="title"]/@content').extract()[0]
        v['url'] = hxs.select('/html/head/link[@rel="video_src"]/@href').extract()[0]
        v['desc'] = hxs.select('/html/head/meta[@name="description"]/@content').extract()[0].strip()
        v['thumbnail'] = hxs.select('/html/head/link[@rel="videothumbnail"]/@href').extract()[0]
        v['tags'] = None
        v['duration'] = None
        v['externalID'] = hxs.select('/html/head/link[@rel="canonical"]/@href').re(r'video:([0-9]*)$')[0]
        return v
        
SPIDER = CHSpider()
