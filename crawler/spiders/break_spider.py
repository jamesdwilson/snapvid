from scrapy.selector import HtmlXPathSelector
from items import Video
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor

class BreakSpider(CrawlSpider):
    name = 'break.com'
    domain_name = 'break.com'
    allowed_domains = ['break.com']
    start_urls = ['http://www.break.com/1']

    rules = (Rule(SgmlLinkExtractor(allow=(r'http://www.break.com/[0-9]*$'))),
             Rule(SgmlLinkExtractor(allow=(r'/index/')),
                  callback='parse_item'),
            )

    def parse_item(self, response):
        hxs = HtmlXPathSelector(response)
        
        v = Video()
        v['pageUrl'] = response.url
        v['title'] = hxs.select('/html/head/meta[@name="embed_video_title"]/@content').extract()[0]
        v['url'] = hxs.select('/html/head/meta[@name="embed_video_url"]/@content').extract()[0]
        v['desc'] = hxs.select('/html/head/meta[@name="embed_video_description"]/@content').extract()[0].strip()
        v['thumbnail'] = hxs.select('/html/head/meta[@name="embed_video_thumb_url"]/@content').extract()[0]
        v['tags'] = None
        v['duration'] = None
        v['externalID'] = hxs.select('/html/head/meta[@name="embed_video_url"]/@content').re(r'break.com/([0-9]*)$')[0]
        return v
        
SPIDER = BreakSpider()
