from scrapy.selector import HtmlXPathSelector
from items import Video
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor

class EbaumsSpider(CrawlSpider):
    name = 'ebaumsworld.com'
    domain_name = 'ebaumsworld.com'
    allowed_domains = ['ebaumsworld.com']
    start_urls = ['http://www.ebaumsworld.com/']

    rules = (#Rule(SgmlLinkExtractor(allow=(r'/video/'))),
             Rule(SgmlLinkExtractor(allow=(r'/video/watch/[0-9]*?/')),
                  callback='parse_item',
                  follow=True),
            )

    def parse_item(self, response):
        hxs = HtmlXPathSelector(response)
        
        v = Video()
        v['pageUrl'] = response.url
        v['title'] = hxs.select('/html/head/meta[@name="title"]/@content').extract()[0]
        v['url'] = hxs.select('/html/head/link[@rel="video_src"]/@href').extract()[0]
        v['desc'] = hxs.select('/html/head/meta[@name="description"]/@content').extract()[0].strip()
        v['thumbnail'] = hxs.select('/html/head/link[@rel="videothumbnail"]/@href').extract()[0]
        v['tags'] = None
        v['duration'] = None
        v['externalID'] = hxs.select('/html/head/link[@rel="video_src"]/@href').re(r'video/watch/([0-9]*?)/')[0]
        return v
        
SPIDER = EbaumsSpider()