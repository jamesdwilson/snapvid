from scrapy.selector import HtmlXPathSelector
from items import Video
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor

class MetacafeSpider(CrawlSpider):
	name = 'metacafe.com'
	domain_name = 'metacafe.com'
	allowed_domains = ['metacafe.com']
	start_urls = ['http://www.metacafe.com/videos/ever/page-1/']

	rules = (
			Rule(SgmlLinkExtractor(allow=(r'/videos/ever/page-[0-9]*?'))),
			Rule(SgmlLinkExtractor(allow=(r'/watch/*/*/')),
									callback='parse_item'),
			)

	def parse_item(self, response):
		hxs = HtmlXPathSelector(response)
		v = Video()
		v['pageUrl'] = response.url
		v['title'] = hxs.select('/html/head/meta[@name="title"]/@content').extract()[0]
		v['url'] = hxs.select('/html/head/link[@rel="video_src"]/@href').extract()[0]
		v['desc'] = hxs.select('/html/head/meta[@name="description"]/@content').extract()[0].strip()
		v['thumbnail'] = hxs.select('/html/head/link[@rel="image_src"]/@href').extract()[0]
		v['tags'] = None
		v['duration'] = None
		v['externalID'] = hxs.select('/html/head/link[@rel="canonical"]/@href').re(r'http://www.metacafe.com/watch/([0-9]*?)/[a-z]*')[0]
		return v

SPIDER = MetacafeSpider()
