from django.conf.urls.defaults import *
from django.http import HttpResponse
#import openid_auth

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns( '',
    (r'^robots\.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /", mimetype="text/plain")),
    ( r'^$', 'src.search.views.index' ),
    ( r'^user/login$', 'users.views.login' ),
    ( r'^user/logout$', 'users.views.logout' ),
    ( r'^user/register$', 'users.views.register' ),
    ( r'^user/savesettings$', 'users.views.save_settings' ),
    ( r'^user/getsettings$', 'users.views.get_settings' ),
    ( r'^user/forgotpassword$', 'users.views.forgot_password' ),
    ( r'^user/share$', 'users.views.share' ),
    ( r'^accounts/login/', 'django.contrib.auth.views.login'),
    ( r'^search/(?P<params>.*)$', 'search.views.query' ),
    ( r'^snapvid.js$', 'search.views.snapvid_js' ),
    ( r'^playlists/$', 'playlists.views.index' ),
    ( r'^playlists/add/$', 'playlists.views.add' ),
    ( r'^playlists/update/$', 'playlists.views.update_playlist_name' ),
    ( r'^playlists/addItems/$', 'playlists.views.addItems' ),
    ( r'^playlists/getPlaylistItems/(?P<playlistid>.*)$', 'playlists.views.getPlaylistItems' ),
    ( r'^playlists/deletePlaylist/$', 'playlists.views.deletePlaylist' ),
    ( r'^playlists/deletePlaylistItems/$', 'playlists.views.deletePlaylistItems' ),
    ( r'^favicon\.ico$', 'django.views.generic.simple.redirect_to', {'url': '/static/img/logo-icon.png'}),
    ( r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': 'static'} ),
    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    #( r'^admin/doc/', include( 'django.contrib.admindocs.urls' ) ),
    ( r'^admin/', include( admin.site.urls ) ),
    (r'^password_change/done/$', 'django.contrib.auth.views.password_change_done'),
    (r'^password_reset/$', 'django.contrib.auth.views.password_reset'),
    (r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done'),
    (r'^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm'),
    (r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete'),
 )
