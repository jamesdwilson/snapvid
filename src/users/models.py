from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True)
    hd = models.BooleanField(default=True)
    adult = models.BooleanField(default=False)
    sources = models.CharField( max_length = 300, null = True, blank = True )
    theme = models.CharField( max_length = 50, null = True, blank = True )