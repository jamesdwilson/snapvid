from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.utils import simplejson
from django.contrib.auth import login as auth_login
from django.contrib.auth import authenticate
from gravatar.templatetags import gravatar
import sys
from django.contrib.auth.views import logout as site_logout, password_reset, password_reset_confirm
from django.contrib.auth.models import User
import string
from random import Random, random
from email.mime.text import MIMEText
from users.models import UserProfile
import logging
import traceback
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from playlists.views import getPlaylistsByUser
from search.engines import engines_friendly
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import int_to_base36
from django.db.models import Q
from django.template import Context, loader
from django.contrib.sites.models import Site


def get_settings_by_user(user):
    settings = {}
    settingsObj = None
    try:
        try:
            settingsObj = user.get_profile() #TODO: change to settings
        except: #Default settings when no settings found
            settingsObj = UserProfile(user=user)
            settingsObj.hd = 1
            settingsObj.adult = 0
            settingsObj.sources = engines_friendly

        settings["first_name"] = user.first_name
        settings["last_name"] = user.last_name
        settings["username"] = user.username
        settings["email"] = user.email
        settings["hd"] = settingsObj.hd
        settings["adult"] = settingsObj.adult
    except:
        logging.error("users.get_settings_by_user(): " + 
                      traceback.format_exc())
    return settings


@login_required
def get_settings(request):
    result = get_settings_by_user(request.user)
    return HttpResponse(simplejson.dumps(result, cls=DjangoJSONEncoder))


def register(request):
    user_email = request.POST["email"]
    try:
#        TODO?: user existence of a session at this point as a red flag
#        why would they have a session and they are trying to register?
        if len(user_email) == 0:
            return

        if User.objects.filter(email=user_email).count() > 0:
            return HttpResponse(simplejson.dumps({"error": "Email already exists"}, cls=DjangoJSONEncoder))
        random_password = User.objects.make_random_password()
        new_user = User.objects.create_user(user_email, user_email,
                                            random_password)

    #create new custom profile for the user
        tprof = UserProfile(user=new_user)
        tprof.save()
    #Mail new password
    #TODO: Replace with rich HTML rendered template
        msgSubject = 'Welcome to SnapVid! Account password inside.'
        msgText = ("Welcome to SnapVid!\n\nUsername: " + user_email + 
                   "\nPassword: " + random_password + "\n\nYou can change \
                   both after you login by clicking on your username: \
                   http://snapvid.tv:8000\n\nThanks!\n-SnapVid.TV Team")
        msgFrom = '"SnapVid.TV" <noreply@snapvid.tv>'
        msgTo = user_email

        send_mail(msgSubject, msgText, msgFrom, [msgTo], fail_silently=False)
    #TODO: return default profile?
        return HttpResponse(simplejson.dumps({"success": 1},
                                             cls=DjangoJSONEncoder))
    except:
        logging.error("users.register(): " + traceback.format_exc())
        return HttpResponse(simplejson.dumps({"error": "Unexpected error,\
         please contact support."}, cls=DjangoJSONEncoder))


def logout(request):
    try:
        if request.session['csrf_token'] != request.POST['csrf_token']:
            raise Exception('Invalid CSRF token')
        site_logout(request)
    except:
        logging.error("users.logout(): " + traceback.format_exc())
    return HttpResponse("")


def login(request):
    result = {}
    try:
#        if request.session['csrf_token'] != request.POST['csrf_token']:
#           raise Exception('Invalid CSRF token')

        user = authenticate(username=request.POST["u"],
                             password=request.POST["p"])
        if user  is not None:
            if user.is_active:
                auth_login(request, user)
                result["username"] = user.username
                result["playlists"] = getPlaylistsByUser(user)
                result["icon"] = gravatar.gravatar_for_email(user.email,
                                                              size=16)
                if 'csrf_token' not in request.session:
                    csrf_token = ''.join(random.choice(string.letters + string.digits) for i in xrange(25))
                    request.session['csrf_token'] = csrf_token
                result['csrf_token'] = request.session['csrf_token']
                result["settings"] = get_settings_by_user(user)
            else: # Return a "disabled account" error message
                result["error"] = "Account disabled. Please contact support\
                 for possible reinstatement."
        else:
            # Return an "invalid login" error message.
                result["error"] = "invalid"
    except:
        logging.error("users.register(): " + traceback.format_exc())
        result["error"] = 1
    return HttpResponse(simplejson.dumps(result, cls=DjangoJSONEncoder))

@login_required
def save_settings(request):
    result = {"success": 0}
    userConflictNameEmail = {}
    
    try:
        if request.session['csrf_token'] != request.POST['csrf_token']:
            raise Exception('Invalid CSRF token')
        
        user = request.user
        #Update user object
        #TODO: validation username = forms.RegexField(label=_("Username"),
        #max_length=30, regex=r'^[\w.@+-]+$',
        user.username = request.POST["username"]
        user.first_name = request.POST["first_name"]
        user.last_name = request.POST["last_name"]
        #TODO: email validation
        #TODO: check for duplicate emails
        user.email = request.POST["email"]
        
        if "email" in request.POST and user.email != request.POST["email"] and User.objects.filter(email=user.email).count() > 0:
            userConflictNameEmail = User.objects.get(Q(email=request.POST["email"]) | 
                            Q(username=request.POST["email"]))
            if userConflictNameEmail is not None:
                if userConflictNameEmail != user or user.username != userConflictNameEmail.username:
                    return HttpResponse(simplejson.dumps({"error": "Email already exists"}, cls=DjangoJSONEncoder))

        #Update settings of user
        try:
            settings = user.get_profile()
        except:
            settings = UserProfile(user=request.user) #Create new settings obj
            #TODO: rename to settings
        settings.hd = (request.POST["hd"] == "1")
        settings.adult = (request.POST["adult"] == "1")
        #settings.sources = request.POST["sources"] #Comma delimited

    #settings.theme = request.POST["theme"]

        if ("newPassword" in request.POST) and len(request.POST["newPassword"]) > 5:
            #TODO: validation against password business rules before saving
            #TOOD: validate current password is correct before allowing the
            #setting of a new password
            current_password = request.POST["currentPassword"]
            if user.check_password(current_password) == False:
                logging.error("users.saveSettings(): " + traceback.format_exc())
                result["error"] = 1
                return HttpResponse(simplejson.dumps(result, cls=DjangoJSONEncoder))
            #all good, set password
            new_password = request.POST["newPassword"]
            user.set_password(raw_password=new_password)
        
        #If there is an error above, neither user properties nor settings
        #will be saved
        user.save()
        settings.save()
        result["success"] = "1"
    except:
        logging.error("users.save_settings(): " + traceback.format_exc())
        result["error"] = 1 #TODO: Error logging
    return HttpResponse(simplejson.dumps(result, cls=DjangoJSONEncoder))


def forgot_password(request):
    result = {"success": 0}
    try:
#        if 'csrf_token' not in request.session:
#            raise Exception('CSRF token not in session!')
#        if request.session['csrf_token'] != request.POST['csrf_token']:
#           logging.error('Error while parsing CSRF token in forgot password. REAL CSRF token = \'' + request.session['csrf_token'])
#           if 'csrf_token' in request.POST:
#               logging.error('SENT CSRF token=\'' + request.POST['csrf_token'])
#           raise Exception('Invalid CSRF token')

        user = User.objects.get(Q(email=request.POST["email"]) | 
                                Q(username=request.POST["email"]))
        if user is not None:
            token_generator = default_token_generator
            t = loader.get_template('users/password_reset_email.html')
            uid = int_to_base36(user.id)
            token = token_generator.make_token(user)
            current_site = Site.objects.get_current()
            site_name = current_site.name
            domain = current_site.domain
            c = {'email': user.email,
                'domain': domain,
                'site_name': site_name,
                'uid': uid,
                'user': user,
                'token': token,
            }
            logging.debug("users.forgot_password(): resetting password for user: "
                          + str(user.id) + ", email: " + user.email)
            send_mail("Password reset on SnapVid.TV",
                        t.render(Context(c)), None, [user.email])
            result["success"] = 1
        else:
            result = {"success": 0, "error": "no user found"}
    except:
        logging.error("users.forgot_password(): " + traceback.format_exc())
        result = {"success":0, "error":1}
    return HttpResponse(simplejson.dumps(result))

def share(request):
    result = {"success": 0}
    try: #if they are logged in, give them csrf protection
        if 'csrf_token' in request.session and request.session['csrf_token'] != request.POST['csrf_token']:
           raise Exception('Invalid CSRF token')

        target_emails = request.POST['shareEmail']
        user_abbreviated = (request.user.first_name + " " + request.user.last_name).strip() or request.user.email or "anonymous"
        
        videoTitle = request.POST['title']
        t = loader.get_template('users/share_email.html')
        current_site = Site.objects.get_current()
        site_name = current_site.name
        domain = current_site.domain
        c = {   'user_abbreviated':user_abbreviated,
                'videoTitle': videoTitle,
                'snapvidId': request.POST['snapvidId'],
                'domain': domain,
                'site_name': site_name,
        }
        
        logging.debug("users.share(): " + user_abbreviated + " sharing video '" + videoTitle + "' for: " + target_emails)
        send_mail(user_abbreviated + " wants you to see " + videoTitle,
                    t.render(Context(c)), None, target_emails.split(',')) #TODO: BCC or loop http://docs.djangoproject.com/en/dev/topics/email/#django.core.mail.EmailMessage
        result["success"] = 1
    except:
        logging.error("users.share(): " + traceback.format_exc())
        result = {"success":0, "error":1}
    return HttpResponse(simplejson.dumps(result))
