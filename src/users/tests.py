"""
User registration:

Set up the client:
>>> from django.test.client import Client
>>> import re
>>> c = Client()
>>> snapvidjs = c.get('/snapvid.js').content
>>> csrf_token = re.search(".*var csrf_token = '(.*?)'.*", snapvidjs).groups(1)[0]

Registering with unique email:
>>> c.post('/user/register', {'csrf_token':csrf_token, 'email': 'valid@snapvid.tv'}).content
'{"success": 1}'

No duplicates allowed: if that email is already in the system, you will get an error:
>>> c.post('/user/register', {'csrf_token':csrf_token, 'email': 'valid@snapvid.tv'}).content
'{"error": "Email already exists"}'

#Registering with unique email testing for CSRF:
#>>> c.post('/user/register', {'csrf_token':csrf_token + 'this_should_fail', 'email': 'alsovalid@snapvid.tv'}).content != '{"success": 1}'
#True


User login:

Setup test data:
>>> from django.contrib.auth.models import User
>>> from users.models import UserProfile
>>> import json
>>> test_user = User.objects.create_user('test@jameswilson.name', 'test@jameswilson.name', 'TESTPASS')
>>> UserProfile(user=test_user).save()

Successful login:
>>> json.loads(c.post('/user/login', {'csrf_token':csrf_token, 'u': 'test@jameswilson.name', 'p':'TESTPASS'}).content).keys()
[u'username', u'csrf_token', u'icon', u'playlists', u'settings']

Unsuccessful login:
>>> c.post('/user/login', {'csrf_token':csrf_token, 'u': 'test@jameswilson.name', 'p':'BADPASS'}).content
'{"error": "invalid"}'

#Unsuccessful login due to invalid CSRF:
#>>> c.post('/user/login', {'csrf_token':csrf_token + 'this_should_fail', 'u': 'test@jameswilson.name', 'p':'TESTPASS'}).content
#'{"error": 1}'

"""