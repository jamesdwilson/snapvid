//Coded by Cory Dambach, Inspired by the C# used in the Mono Projects Developer Blog

this.TemplateRenderer = function( parentEl )
{
	this.templatedEl		= parentEl;
	this.templates			= new Array(); //will be assigned to again later, see GetTemplates
	this.currentTemplate	= 0;
			
	this.Add = function( nvc, maxKeyLen ) {
		var filledTemplate = this.FillTemplate( this.GetNextTemplate(), nvc, maxKeyLen );
		filledTemplate.appendTo( this.templatedEl );		
	}
	   	
	this.Clear = function() {
		//this.templatedEl.style.display = "none";
		
		var newParent = this.templatedEl.cloneNode( true );
		document.replaceChild( newParent, this.templatedEl );
		this.templatedEl = newParent;
		return;
		
		while( this.templatedEl.childNodes.length > 0 )
			$( this.templatedEl.firstChild ).remove();			
		
		if( this.templatedEl.rows )
		{
			if( this.templatedEl.rows.length == 0 ) //this is actually for a wierd firefox behavior
			{
				while( this.templatedEl.firstChild.rows.length != 0 )
					this.templatedEl.firstChild.rows.deleteRow( 0 );
				return;
			}
			else
			{
				while( this.templatedEl.rows.length != 0 )
					this.templatedEl.rows.deleteRow( 0 );
				return;
			}
		}
		this.templatedEl.style.display = "";
	};
	
	this.FillTemplate = function( template, valueNVC, maxKeyLen ) //Works in Firefox2, and IE7
	{			
		if( !maxKeyLen ) //then it must be an ad-hoc thing		
			maxKeyLen = valueNVC.GetLongestKeyLength();		
		
		template.innerHTML = TemplateRenderer.FillString( template.innerHTML, valueNVC, maxKeyLen ); //TODO: might want to loop over all variables in the template element itself
		return template; //horribly inneficient, but will be dealing with small amounts of data on the client side, so, let them eat cake
	};
		
	this.GetNextTemplate = function() {
		var nextTemplate = this.GetTemplate( this.currentTemplate )
		this.currentTemplate++;
		return nextTemplate;
	}
		
	this.GetTemplate = function( i ) //a function that returns the appropriate template with respect to the index of the destination child
	{
		if( !i )
			i = 0;
			
		var template = this.templates[i % this.templates.length];
		template= template.clone(false); //deleted line that replaced "Template" in className with nothing
		return template; //now it is ready to be used.
	};
	
	//the below method is private, well it isn't really, but it should not be called after more children have been added
	this.GetTemplates = function() //get any templates that this HTML Element contains, Tested in FireFox2 and IE7, Works!
	{
		var el			= this.templatedEl; //alias the needed var, less typing
		var els			= el.getElementsByTagName( "*" ); //get all child elements
		var templates	= new Array(); //<HTMLElements>, an array of HTMLElements that correspond to templates
		for( var i = 0; i < els.length; i++ )
		{
			//foreach element in the resulting list, get the classname value of this element
			var className = els[i].className;
			if( (!className) || className == "" ) //check to see if null, undefined, or empty string
				continue; //move on to the next element		
			else if( className.indexOf( "Template" ) > -1 ) //check for all elements with a class of template
				templates.push( els[i] ); //add the element 
		}
		return templates;
	};		
	
	this.RemoveTemplates = function() {
		for( var i = 0; i < this.templates.length; i++ )
			$( this.templates[i] ).remove();
		return;
	};
	
	this.templates	= this.GetTemplates( this.templatedEl ); //assign to our templates	
	var tempDiv		= document.createElement( "DIV" );
	for( var i = 0; i < this.templates.length; i++ )
		$( this.templates[i] ).appendTo( tempDiv );		
	//now we are good to go!
}

TemplateRenderer.FillString = function( text, valueNVC ) //Works in Firefox2, and IE7
{
	for( var i = 0; i < valueNVC.Keys.length; i++ )	
		while( text.indexOf( '{' + valueNVC.Keys[i] + '}' ) > -1 ) //if the {string} still exists in the HTML
			text = text.replace( "{" + valueNVC.Keys[i] + "}", valueNVC.Get( valueNVC.Keys[i] ) ); //get by index );
	return text; //horribly inneficient, but will be dealing with small amounts of data on the client side, so, let them eat cake
};

TemplateRenderer.RegexFillString = function( text, valueNVC, maxKeyLen )
{

}

