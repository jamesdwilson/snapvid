
/**
 * Allows event handlers to have more than one action assigned to them
 */
function MethodGroup () { 

	this.run = function ( /* uses arguments special variable */ ) { //arguments are gotten via the arguments property
		var results /* : Array */ = new Array(); 
		
		for( var i = 0; i < this.length; i++ )
		{
			var obj = new Object(); //probably taboo
			
			if( this[i] instanceof MethodGroup )
				this[i].run.apply( this[i], arguments );
			else
				this[i].apply( obj, arguments ); //hack, we are getting rid of the o when we are done with it
			results.push( obj ); //return the results of the event handlers as an array 
		}
	}
}
MethodGroup.prototype = Array.prototype;