
function ItemTemplate( templateElement )
{	
	this.template = templateElement;	
	this.render = function( data ) {
		template.innerHTML = ItemTemplate.fillString( template.innerHTML, data ); //TODO: might want to loop over all variables in the template element itself
		return template; //horribly inefficient, have a better algorithm but not with me right now
	};
}

ItemTemplate.fillString = function( text, data ) //Works in Firefox2, and IE7
{
	for( var p in data ) //for each propName in data 
		while( text.indexOf( '{' + p + '}' ) > -1 ) //if the {string} still exists in the HTML
			text = text.replace( "{" + p + "}", data[p] ); //get by index );
	return text; //horribly inefficient, but will be dealing with small amounts of data on the client side, so, let them eat cake
};
