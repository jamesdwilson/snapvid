
/**
 * Goals for this class are to inherit from Array 
 * and to render trigger event handlers when changes are made 
 */
function DataList () {
	
	if( arguments.length > 1 ) //we aren't implementing the Array(len) constructor option, just out of convenience
		for( var i = 0; i < arguments.length; i++ )		
			this.push( arguments[i] );
	
	//back up the original methods, making them private
	var _push		= this.push;
	var _pop		= this.pop;
	var _reverse	= this.reverse;
	var _shift		= this.shift;
	var _sort 		= this.sort;
	//no change needed here, nothing to override	
	
	this.onPush		= new MethodGroup();
	this.onPop		= new MethodGroup();
	this.onReverse	= new MethodGroup();
	this.onShift	= new MethodGroup();
	this.onSort		= new MethodGroup();
	this.onChange	= new MethodGroup();
	
	this.push = function() {
		_push.apply( this, arguments ); 	//equivalent to super.push( ... ) or base.push( ... )
		this.onPush.run();
	}
	
	this.pop = function() {
		_pop.apply( this, arguments ); 		//equivalent to super.pop( ... ) or base.pop( ... )
		this.onPop.run();
	}
	
	this.reverse = function() {
		_reverse.apply( this, arguments );	//equivalent to super.reverse( ... ) or base.reverse( ... )
		this.onReverse.run();
	}
	
	this.shift = function() {
		_shift.apply( this, arguments );	//equivalent to super.shift( ... ) or base.shift( ... )
		this.onShift.run();
	}
	
	this.sort = function() {
		_sort.apply( this, arguments );		//equivalent to super.sort( ... ) or base.sort( ... )
		this.onSort.run();
	}

	var eventMethodGroups = [this.onPush, this.onPop, this.onReverse, this.onShift, this.onSort]; //change is excluded from this for obvious reasons
	for( var i = 0; i < eventMethodGroups.length; i++ )			
		eventMethodGroups[i].push( this.onChange ); //on change should be.runed after any one of these	
}
DataList.prototype = Array.prototype; //inherit from array

DataList.RunUnitTests = function() //static
{
	function Control( listRef ) {
		listRef.onChange.push( function() { /* re-render the controls contents */ } );
	}
	
	var l = new DataList();
	l.onPush.push(function(){
		alert("array was changed");
	});
	l.push("do something");
	
	var l2 = new DataList();
	
	l2.onChange.push(function(){
		var j = 4;
		var i = j;
		var k = i - j;
	} );
	
	for (var i = 0; i < 5; i++) {
		l2.push(Math.random() * 100);
	}

	l2.sort();
}

DataList.RunUnitTests();
