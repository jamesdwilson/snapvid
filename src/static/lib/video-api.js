/*members addEventListener, init, onReady, onStateChange, pause, 
 pauseVideo, play, playVideo, stop, stopVideo, trigger
 */
/*jslint browser: true, devel: true, undef: true, nomen: true, eqeqeq: true, plusplus: true, bitwise: true, regexp: true, newcap: true, immed: true, strict: true */
/**
 * Abstracted player API for multiple flash players as well as HTML5
 * @classDescription Abstracted player API for multiple flash players as well as HTML5
 * @author Dambach-Wilson
 * @copyright 2010, All Rights Reserved
 * @constructor
 * @version 1.0
 */
'use strict';
function Player() {

    /**
     * Player type - example: youtube, moogaloop, html
     * @type {string}
     */
    this.playerType = null;
	
    /**
     * Player of specific type's internal object reference
     * @type {object}
     */
    this.playerOfType = null;
    
    /**
     * Player state - example: playing
     * @type {string}
     * @private
     */
     var playerState,
    
    /**
     * direct reference to flash or HTML video object
     * @private
     * @type {object}
     * @memberOf Player
     */
      player;
    
    /**
     * Initializes the player depending on the video type
     * @method
     * @param {string} playerTypeParam  moogaloop, youtube, etc
     * @param {string} elementId HTMLElement string id to container
     * @param {object} params additional parameters such as event hooks
     * @memberOf Player
     * @exception {ReferenceError} Throws error of type ReferenceError with message == "elementId not found"
     * @exception TypeError Throws error of type TypeError if passed an unexpected playerType
     * @return {object} any return values such as event hooks
     */
    this.init = function( playerTypeParam, elementId, params )	{	
  		var playerTypes = { "youtube": youtube, "moogaloop": moogaloop }, $element;		
  	  
  		log('Player.init()');
    
  		if(typeof elementId === 'undefined' || elementId.length === 0) {
  			throw new ReferenceError("elementId not found: " + elementId);
      }
      $element =  $('#' + elementId);
      if($element.length === 0) {
        throw new ReferenceError("elementId not found: " + elementId);
      }
    
  		//re-use existing player object if possible
    	if(playerTypeParam === this.playerType && typeof this.playerOfType.setVideo !== 'undefined') { 
        log('reusing ' + playerTypeParam);
        return this.playerOfType.setVideo(params);
      }
      else { //create new player
        log('creating new ' + playerTypeParam);
        this.playerOfType = new playerTypes[playerTypeParam]();
        player = $element[0];
        this.playerType = playerTypeParam;
        return this.playerOfType.init(params); //pass any variables and return any required external hooks
      }
	};
	
	//State changing functions	
	
	/**
	 * Stops player
	 * @method
	 * @memberOf Player
	 */
	this.stop = function() {
		log('player.stop()');
		playerOfType.stop();
	};
	
	/**
	 * Plays player
	 * @method
	 * @memberOf Player
	 */
	this.play = function() {
		log('player.play()');
		playerOfType.play();
	};
	
	/**
	 * Pauses player
	 * @method
	 * @memberOf Player
	 */
	this.pause = function() {
		log('player.pause()');
		playerOfType.pause();
	};
	
	/* Event handlers */
	//I'm not a fan of how the event handler section is implemented 
	//right now, it's not extensible enough for my tastes -Cory D.
	
	/*
	this.onStop		= new Array();
	this.onPlay		= new Array();	
	this.onPause	= new Array();
	this.onReady	= new Array();
	*/	
		   
	/**
	 * onStop event handler - called when a video is stopped
	 * @private
	 * @method
	 * @memberOf Player
	 */
	function onStop(e) {
		log('player.onStop()');
		playerState = 'stopped';
		$('body').trigger('stop.Player', {/*TODO: pass event details */});
	}
	
	/**
	 * onPlay event handler - called when a video is played
	 * @private
	 * @method
	 * @memberOf Player
	 */
	function onPlay(e) {
		log('player.onPlay()');
		playerState = 'playing';
		$('body').trigger('play.Player', {/*TODO: pass event details */});
	}
	
	/**
	 * onPause event handler - called when a video is paused
	 * @private
	 * @method
	 * @memberOf Player
	 */
	function onPause(e) {
		log('player.onPause()');
		playerState = 'paused';
		$('body').trigger('pause.Player', {/*TODO: pass event details */});
	}
	
	/**
	 * onReady event handler - called when a video is ready
	 * @private
	 * @method
	 * @memberOf Player
	 */
	function onReady(e) {
		log('player.onReady()');
		playerState = 'stopped';
		$('body').trigger('ready.Player', {/*TODO: pass event details */});
	}
	
	/**
	 * onFinish event handler - called when a video is finished
	 * @private
	 * @method
	 * @memberOf Player
	 */
	function onFinish(e) {
		log('player.onFinish()');
		playerState = 'stopped';
		$('body').trigger('finish.Player', {/*TODO: pass event details */});
	}
	
	/// Player Implementations	
	/**
	 * moogaloop Player Implementation
	 * @classDescription moogaloop Player Implementation
	 * @see http://www.vimeo.com/api/docs/moogaloop
	 * @constructor
	 * @memberOf Player
	 * @private
	 */
	var moogaloop = function() {
		/**
		 * Params - set in init(), to handle external hooks
		 * @private
		 * @type {object}
		 */
		var paramsObj = {};
		
		this.onPlay = function() {
			log('moogaloop.onPlay()');
			onPlay();
		};
		this.onPause = function() {
			log('moogaloop.onPause()');
			onPause();
		};
		this.onFinish = function() {
			log('moogaloop.onFinish()');
			onFinish(); //this doesn't call this.onFinish but instead calls the lexically scoped onFinish of the parent class.
		};
		
		/**
		 * Called indirectly by moogaloop flash player when player is initialized and ready for js calls
		 * @memberOf moogaloop
		 * @method
		 */
		this.onReady = function() {
			log('moogaloop.onReady()');
      log('moogaloop.onReady() - bound events');
			onReady();
		};
				
		/**
		 * Stops moogaloop player
		 * @memberOf moogaloop
		 * @method
		 */
		this.stop = function() {
			log('moogaloop.stop()');
			this.pause();
		};
		
		/**
		 * Plays moogaloop player
		 * @memberOf moogaloop
		 * @method
		 */
		this.play = function() {
			log('moogaloop.play()');
			player.api_play();
		};
		
		/**
		 * Pauses moogaloop player
		 * @memberOf moogaloop
		 * @method
		 */
		this.pause = function() {
			log('moogaloop.pause()');
			player.api_pause();
		};
		
		/**
		 * Initializes the moogaloop javascript API and registers event handlers
		 * @memberOf moogaloop
		 * @method
		 * @param {object} params - event hooks
		 * @return {object} event hooks
		 */
		this.init = function(params) {
			log('moogaloop.init()');
 		  window['moogaloop_onFinish'] = this.onFinish;
 		  window['moogaloop_onPlay'] = this.onPlay;
 		  window['moogaloop_onPause'] = this.onPause;
 		  player.api_addEventListener('onFinish', 'moogaloop_onFinish');
      player.api_addEventListener('onPlay', 'moogaloop_onPlay');
      player.api_addEventListener('onPause', 'moogaloop_onPause');
  		log('moogaloop.init() - bound events');
		};
		
	};
	//end moogaloop implementation
	
	/**
	 * YouTube Player Implementation
	 * @classDescription YouTube Player Implementation
	 * @see http://code.google.com/apis/youtube/js_api_reference.html
	 * @constructor
	 * @memberOf Player
	 * @private
	 */
	var youtube = function() {
		/**
		 * Params - set in init(), to handle external hooks
		 * @private
		 * @type {object}
		 */
		var paramsObj = {};
		
		var youTubeStates = {
			unstarted: -1,
			finished: 0,
			playing: 1,
			paused: 2,
			buffering: 3,
			stopped: 5		
		};		
		
		/**
		 * Called by youtube's JSAPI upon a state change, normalizes and maps to local state
		 * @memberOf youtube
		 * @method
		 */
		this.onStateChange = function(newState) {
		
			switch (newState) {
				case -1: //unstarted
					onStop();
					break;
				case 0:
					onFinish();
					break;
				case 1: //playing
					onPlay();
					break;
				case 2: //paused
					onPause();
					break;
				case 3: //buffering
					onPause();
					break;
				case 5: //stopped
					onStop();
					break;
				default: //unexpected
					log('youtube.onStateChange(): UNEXPECTED newState: ' + newState);
			}
		};
		
		/**
		 * Called indirectly by youtube flash player when player is initialized and ready for js calls
		 * @memberOf youtube
		 * @method
		 */
		this.onReady = function() {
			log('youtube.onReady()');
			/* TODO: expose onyoutubeStateChange to player. The youtube docs suggest only allow passing a function name by string (not by reference):
			 * player.addEventListener(event:String, listener:String):Void
			 * Adds a listener function for the specified event. The Events section below identifies the different events that the player might fire.
			 * The listener is a string that specifies the function that will execute when the specified event fires.
			 * */
			//			log(paramsObj);
			log('youtube.onReady() - binding onStateChange');
			player.addEventListener('onStateChange', 'onStateChange');
			log('youtube.onReady() - bound onStateChange');
			onReady();
			
		};
		
		
		/**
		 * Stops youtube player
		 * @memberOf youtube
		 * @method
		 */
		this.stop = function() {
			log('youtube.stop()');
			player.stopVideo();
		};
		
		/**
		 * Plays youtube player
		 * @memberOf youtube
		 * @method
		 */
		this.play = function() {
			log('youtube.play()');
			player.playVideo();
		};
		
		/**
		 * Pauses youtube player
		 * @memberOf youtube
		 * @method
		 */
		this.pause = function() {
			log('youtube.pause()');
			player.pauseVideo();
		};
		
		/**
		 * Sets youtube video to new video without reloading
		 */
		this.setVideo = function(params) {
		  player.loadVideoById(params['id'], 0, 'highres'); //TODO: pass video quality  
		}
		
		/**
		 * Initializes the youtube javascript API and registers event handlers
		 * @memberOf youtube
		 * @method
		 * @param {object} params additional parameters such as event hooks
		 * @return {object} event hooks
		 */
		this.init = function(params) {
			log('youtube.init()');
			paramsObj = params;
			window['onYouTubePlayerReady'] = this.onReady;
			window['onStateChange'] = this.onStateChange;
		};
		
	};
	//End youtube implementation	
}
