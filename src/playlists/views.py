"""Views page for Favorites App"""
from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.core.exceptions import ObjectDoesNotExist
from models import Playlist, PlaylistItem
import json
import sys
from django.utils import simplejson
from django.core.serializers.json import DjangoJSONEncoder
import logging
import traceback
from django.contrib.auth.decorators import login_required
import django
from django.contrib.auth.models import AnonymousUser
import re, string

LINE_DELIMITER = '\r'
DELIMITER = '|'


def short_url(url):
    if url[:7] == "http://":
        url = url[7:]
    if url[:4] == "www.":
        url = url[4:]
    return url

def serializeVideo(user, video):
    video_serialized = ''
    domain_name = "NO DOMAIN"
    if video.externalID:
        domain_name = video.externalID.split('_')[0]
    site = None
    playlisted = '0'
    playlistitemid = '0'
#   if user and (type(user) is not AnonymousUser) and playlistitemid:
    #   playlistitemid = getPlaylistItemId(user, video) or '0' #TODO: fix
    #   playlistitemid = str(playlistitemid)
    try:
        site = Site.objects.filter(domain_name=domain_name).get()
    except:
        pass
    localsiteid = '0'
    externalid = ''
    thumbnail = ''
    if site is not None:
        localsiteid = site.id
        externalid = video.externalID[len(domain_name) + 1:]
        if site.thumbnail_template is not None and len(site.thumbnail_template) > 0:
            thumbnail = ''
    else:
        externalid = short_url(video.url)
        thumbnail = short_url(video.thumbnail)
    return DELIMITER.join([re.sub('[\||\n|\r]', '-', video.title.strip(string.whitespace + '-.:')), #Jersey Face| 
                                         re.sub('[\|\n|\r]', '-', video.desc.strip(string.whitespace + '-.:')), #Rasheed Wallace giving an assistant coach a souvenir. Right in the mouth.
                                         str(localsiteid) + str(externalid), #31761766|
                                         thumbnail, #2.media.collegehumor.cvcdn.com/5/4/collegehumor.d5120b35ee04c4c9d6ea2a64fa14c95e
                                         playlistitemid #if the user has this video in their playlists
                                         ]).strip(DELIMITER)
def serializeVideos(user, videos):
    videos_serialized = []
    for video in videos:
        videos_serialized.append(serializeVideo(user, video))
    return LINE_DELIMITER.join(videos_serialized)

#returns true if a video with the same url is found in the user's playlists 
def getPlaylistItemId(user, video):
    queryset = Playlist.objects.filter(user=user)
    for result in queryset:
        listitems = PlaylistItem.objects.filter(playlist=result)
        for listitemvideo in listitems:
            if video.title == listitemvideo.title:
                return listitemvideo.id
    return None
    
#returns true if a video with the same url is found in the user's playlists 
def isPlaylisted(user, video):
    queryset = Playlist.objects.filter(user=user)
    for result in queryset:
        listitems = PlaylistItem.objects.filter(playlist=result)
        for listitemvideo in listitems:
            if video['title'] == listitemvideo.title:
                return True
    return False
    
#retrieves the list of Playlists
def getPlaylistcount(user, playlistname):
    playlistcount = 0
    queryset = Playlist.objects.filter(user=user, name=playlistname).order_by("name")
    for result in queryset:
        playlistcount = PlaylistItem.objects.filter(playlist=result).count()
    return playlistcount

#retrieves the list of Playlists
def getPlaylistsByUser(user):
    queryset = Playlist.objects.filter(user=user).order_by("name")
    results = []
    for result in queryset:
        playlistcount = PlaylistItem.objects.filter(playlist=result).count()
        results.append({"title":result.name, "count":playlistcount, "id":result.pk})
    return results

def index(request):
    results = getPlaylistsByUser(request.user)
    return HttpResponse(simplejson.dumps(results))

#retrieves the list of items in a playlist, given the id (PK) of the playlist.
@login_required
def getPlaylistItems(request, playlistid):
    List = Playlist.objects.get(pk=playlistid, user=request.user)
    playlistitems = PlaylistItem.objects.filter(playlist=List).order_by("title")

    serialized_playlistitems = serializeVideos(request.user, playlistitems) 
#    for result in queryset:
#        results.append({"id": result.pk, "title":result.title, "rating":result.rating,
#                         "desc":result.desc, "url": result.url, "pageUrl":result.pageUrl,
#                         "thumbnail":result.thumbnail})
    return HttpResponse(serialized_playlistitems)

#inserts a playlist
@login_required
def add(request):
    try:
        new_list_name = request.POST.get("name")
        if Playlist.objects.filter(name=new_list_name).count() > 0:
            return HttpResponse(simplejson.dumps({"error":"A playlist with that name already exists"}, cls=DjangoJSONEncoder))
        list = Playlist()
        list.user = request.user
        list.name = new_list_name
        list.save()
        result = { "count": Playlist.objects.filter(user=request.user).count(),
                  "id": list.id }
        return HttpResponse(simplejson.dumps(result))
    except:
        logging.error("playlists.add(): " + traceback.format_exc())
        return HttpResponse(simplejson.dumps({"error":"An unexpected error occurred"}, cls=DjangoJSONEncoder))

@login_required
def update_playlist_name(request):
    try:
        pl = Playlist.objects.get(id=request.POST.get("id"))
        pl.name = request.POST.get("name")
        pl.save()
        return HttpResponse(simplejson.dumps({"message":"Playlist name updated"}, cls=DjangoJSONEncoder))
    except:
        logging.error("playlists.update_playlist_name(): " + traceback.format_exc())
        return HttpResponse(simplejson.dumps({"error":"An unexpected error occurred"}, cls=DjangoJSONEncoder))

#adds 1 or more items to a playlist. The objects must be packaged as a JSON array object.
#ex.: [{"id":3,"title":"Item Title6","desc":"Item Description6","cat":"category6","tags":"tag6, tag7, tag8",
#      "pageUrl":"http://example.com/videos.aspx","url":"http://example.com/video6.flv","duration":195,
#      "rating":6,"thumbnail":"http://example.com/video6.png"}]
#{"id":1,"title":"Item Title6","desc":"Item Description6","cat":"category6","tags":"tag6, tag7, tag8","pageUrl":"http://example.com/videos.aspx","url":"http://example.com/video6.flv","duration":195, "rating":6,"thumbnail":"http://example.com/video6.png"} 
@login_required
def addItems(request):
    videoListData = request.POST.get("videolist")
    try:
        videoList = json.JSONDecoder().decode(videoListData)
        playlistitemids = []
        for video in videoList:
            item = PlaylistItem()
            #TODO: parse playlistid, otherwise default to "Favorites" playlist
            try:
                item.playlist = Playlist.objects.get(name="Favorites", user=request.user)
            except Playlist.DoesNotExist:
                favoritesPlaylist = Playlist(name="Favorites", user=request.user)
                favoritesPlaylist.save()
                item.playlist = favoritesPlaylist
            item.title = video["title"]
            item.desc = video["desc"]
            if "cat" in video:
                item.cat = video["cat"]
            if "tags" in video:
                item.tags = video["tags"]
            if "pageUrl" in video:
                item.pageUrl = video["pageUrl"]
            if "url" in video:
                item.url = video["url"]
            if "duration" in video:
                item.duration = video["duration"]
            if "rating" in video:
                item.rating = video["rating"]
            if "thumbnail" in video:
                item.thumbnail = video["thumbnail"]
            item.save()
            playlistitemids.append(item.id)
        result = {"playlistitemids":playlistitemids }
        return HttpResponse(simplejson.dumps(result))
    except:
        logging.error("playlists.addItems(): " + traceback.format_exc())
        return HttpResponse(simplejson.dumps({"error":"Unexpected error, please contact support."}, cls=DjangoJSONEncoder))

    

#deletes a playlist and all its items
@login_required
def deletePlaylist(request):
    #TODO: would this would allow deletion of other users' playlists?? WTF 
    delcount = 0
    try: #TODO: NO SECURITY ON THESE??
        item = Playlist.objects.get(pk=request.POST.get("id"))
        list = PlaylistItem.objects.filter(playlist=item)
        delcount = list.count()
        list.delete()
        item.delete()
    except ObjectDoesNotExist:
        delcount = 0
    result = { "count": delcount }
    return HttpResponse(simplejson.dumps(result))

#deletes 1 or more playlist items sent as a list of comma separated list item pks.
@login_required
def deletePlaylistItems(request):
    #TODO: would this allow deletion of other users' playlist items? 
    list = PlaylistItem.objects.filter(pk__in=request.POST.get("ids").split(","))
    delcount = list.count()
    list.delete()
    result = { "count": delcount }
    return HttpResponse(simplejson.dumps(result))


