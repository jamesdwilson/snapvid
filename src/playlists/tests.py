"""
Playlists:


User login:

Setup test data (not needed if testing with other modules that already set this up)
>>># from django.contrib.auth.models import User
>>># from users.models import UserProfile
>>># test_user = User.objects.create_user('test@jameswilson.name', 'test@jameswilson.name', 'TESTPASS')
>>># UserProfile(user=test_user).save()


-------
Front end tests:
Set up the client:
>>> from django.test.client import Client
>>> c = Client()

Log in/set up user session:
>>> c.post('/user/login', {'u': 'test@jameswilson.name', 'p':'TESTPASS'}).content
'{"username": "test@jameswilson.name", "icon": "http://www.gravatar.com/avatar/8b004706e5c2a145a108023bf0179555/?default=wavatar&amp;s=16", "favorites": 0, "error": ""}'


Add playlist:
>>> c.post('/playlists/add/',{'name':'funny videos'}).content
'{"count": 1}'

Get list of playlists:
>>> c.get('/playlists/').content
'[{"count": 0, "id": 1, "title": "funny videos"}]'

Add an item to the playlist:
>>> videoListData = '[{"id":1,"title":"Item Title6","desc":"Item Description6","cat":"category6","tags":"tag6, tag7, tag8","pageUrl":"http://example.com/videos.aspx","url":"http://example.com/video6.flv","duration":195, "rating":6,"thumbnail":"http://example.com/video6.png"}]'
>>> c.post('/playlists/addItems/',{'videolist':videoListData}).content
'{"count": 1}'

Get list of playlist items:
>>> c.post('/playlists/getPlaylistItems/',{'id':1}).content
'[{"rating": "6", "title": "Item Title6", "url": "http://example.com/video6.flv", "thumbnail": "http://example.com/video6.png", "pageUrl": "http://example.com/videos.aspx", "id": 1, "desc": "Item Description6"}]'

Delete playlist item:
>>> c.post('/playlists/deletePlaylistItems/',{'ids':1}).content
'{"count": 1}'

Double check:
>>> c.post('/playlists/getPlaylistItems/',{'id':1}).content
'[]'

Delete playlist:
>>> c.post('/playlists/deletePlaylist/',{'id':1}).content
'{"count": 0}'

Double check:
>>> c.post('/playlists/').content
'[]'

"""
