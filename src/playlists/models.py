from django.db import models
from django.contrib.auth.models import User

class Playlist( models.Model ):
    """User's video favorite"""
    user = models.ForeignKey( User )
    name = models.CharField( max_length = 100 )

class PlaylistItem( models.Model ):
    playlist = models.ForeignKey(Playlist)
    title = models.CharField( max_length = 100 )
    desc = models.CharField( max_length = 200, null = True, blank = True )
    cat = models.CharField( max_length = 50, null = True, blank = True )
    tags = models.CharField( max_length = 50, null = True, blank = True )
    pageUrl = models.CharField( max_length = 300, blank = True )
    url = models.CharField( max_length = 300, null = True, blank = True )
    duration = models.IntegerField( default = 0 )
    rating = models.CharField( max_length = 10, null = True, blank = True )
    thumbnail = models.CharField( max_length = 300 , null = True, blank = True )
    externalID = models.CharField(unique=True, max_length=100 , null=True, blank=True) #external sites' ID for the video
