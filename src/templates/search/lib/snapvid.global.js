    var csrf_token = '{{csrf_token}}';
		
		/**
     * Wrapper for JS console, could be expanded to be global error handler, log
     * collector
     *
     * @param {string}
     *            s
     */
    function underscoreEscape(s) {
        return s.replace(/ /g, '_');
    }
    
    function underscoreUnescape(s) {
        return s.replace(/_/g, ' ');
    }
    
    function zero(num) {
        return (num < 0) ? 0 : num;
    }

    function playVideo(video/* :video */) {
        log('playVideo(): snapvidId:' + video.snapvidId);
		log('video object:');
		log(video);
        PlayerInterface.init(video);
        
        // Resize
        UI.smallListMode = true;
        UI.onresize(true);
        
        // Toggle display mode - TODO: refactor to UI
        $('.video-desc').hide();
        $('.video-display').css('display', 'block'); // toggles display mode
        // - todo: refactor
        $(".video-display").height($(".results").height());
        
        // Create history item - TODO: find all like this and refactor to
        // namespace function
        document.title = 'SnapVid.TV - "' + video.title + '"';
        location.href = '#' + video.snapvidId.replace(/\//g, "%2F") + '/' + underscoreEscape(video.title);
	    _gaq.push(['_trackPageview', location.pathname + location.search  + location.hash]);
	    $('#nowPlaying').text(video.title);
    }
    
    // TODO: rewrite function, too opaque, also refactor to a namespace
    function parseDelimitedResults(resultsDelimited/* :String */)/* :Array.<object> */ {
        var LINE_DELIMITER = '\r', DELIMITER = '|', resultsLines = resultsDelimited.split(LINE_DELIMITER), returnArray = [], i, line, ext_id, thumbnail, playlistitemid, localsiteid, url, s, playerType, externalid;
        
        for (i = 0; i < resultsLines.length; i += 1) {
            line = resultsLines[i].split(DELIMITER);
            localsiteid = parseInt(line[2].slice(0, 1), 10) - 1; // -1 to
            // account
            // for
            // 0-based
            // index
            ext_id = line[2].slice(1);
            url = 'http://';
            thumbnail = 'http://';
            playlistitemid = line[4];
            
            if (typeof sites[localsiteid] !== "undefined") {
                s = sites[localsiteid].fields;
                playerType = s.player_type;
                url += (s.url_prefix ? s.url_prefix : '') +
                s.url_template.replace(/\{ext_id\}/ig, ext_id) +
                (s.url_suffix ? s.url_suffix : '');
                if (typeof s.thumbnail_template !== "undefined" && s.thumbnail_template.length > 0) {
                    thumbnail += (s.thumbnail_prefix ? s.thumbnail_prefix : '') +
                    s.thumbnail_template.replace(/\{ext_id\}/ig, ext_id) +
                    (s.thumbnail_suffx ? s.thumbnail_suffx : '');
                }
                else {
                    thumbnail += line[3];
                }
            }
            else {
                thumbnail += line[3];
                url += line[2].slice(1);
            }
            returnArray.push({
                playerType: playerType,
                title: line[0],
                desc: line[1],
                url: url,
                thumbnail: thumbnail,
                externalid: ext_id,
                playlisted: (playlistitemid !== "0"),
                playlistitemid: playlistitemid,
                site: s,
                snapvidId: line[2]
            });
        }
        return returnArray;
    }
    
    var $itemTemplate /* : JQuery */, uri;// Templates for lists,
    