function log(s/* :string */) {
    if (typeof window.console !== 'undefined') {
        console.log(s);
    }
}
'use strict';
var main = function($, undefined) {
    {% include "search/lib/snapvid.global.js" %}
	
    var {% include "search/lib/snapvid.urls.js" %},
	{% include "search/lib/snapvid.playerinterface.js" %},
	{% include "search/lib/snapvid.ui.js" %},
	{% include "search/lib/snapvid.dialog.js" %},
	{% include "search/lib/snapvid.search.js" %},
	{% include "search/lib/snapvid.queue.js" %},
	{% include "search/lib/snapvid.list.js" %},
    {% include "search/lib/snapvid.playlists.js" %},
    {% include "search/lib/snapvid.user.js" %};
	
    {% include "search/lib/snapvid.onload.js" %}
};
main(jQuery);