    /**
     * Overall User Interface Management
     *
     * @author Dambach-Wilson
     * @copyright 2010, All Rights Reserved
     */
    UI = {
        /**
         * If we are currently showing a small list (and large video player)
         * then this should be true
         */
        DIALOG_ERROR_EFFECT_OPTIONS: {
            times: 2
        },
        DIALOG_ERROR_EFFECT_SPEED: 40,
        ANIMATION_DELAY: 550, // mainly used for a delay before focusing
        // fields
        smallListMode /* :boolean */: false,
        loaded /* :boolean */: false,
        /**
         * Toggles wait indicators
         *
         * @param {boolean}
         *            busy or not
         */
        showBusy: function(busy/* :boolean */) {
            if (busy) {
                $("body").css("cursor", "wait"); // TODO: throbber?
            }
            else {
                $("body").css("cursor", "");
            }
        },
        onload: function() {
            UI.loaded = true;
        },
		lastSlideChangeTimer: 0,
        configureSlider: function() {
            log('configureSlider()');
            var max = 0, queue = null, queueLength = 0, item = null, buffer = 0;
            // Horizontal
            item = $(".content-queue li", $("#sliderContent"));
            queueLength = (item.length + 1) * parseInt(item.eq(0).children("img").css('width'), 10) - ($(".content-queue li").length * 11);
            
            if (parseInt($(".viewer").css('width'), 10) > queueLength) {
                $("#slider").css('display', 'none');
            }
            else {
                $("#slider").css('display', 'block');
                queue = $(".content-queue", $("#sliderContent"));
                buffer = ($(".content-queue li", $("#sliderContent")).length - 1) * 130;
                queue.css("width", queueLength + buffer);
                $("#slider").slider({
                    max: (zero(queueLength - parseInt($(".viewer", $("#sliderContent")).css("width"), 10))),
                    slide: function(e, ui) {
                        queue.css("left", "-" + ui.value + "px");
                    }
                    
                });
            }
            // Vertical
            if ($(".video-list-items-container").height() > $(".video-list-items").height()) {
                $("#video-list-slider").css('display', 'none');
            }
            else {
                var $videoListSlider = $("#video-list-slider");
				var resList = $('#resultslist');
                $videoListSlider.css('display', 'block');
                var max = ($(".video-list li").length * 28) + 30 - $(".video-list-items-container").height();
                $videoListSlider.slider({
                    orientation: 'vertical',
                    min: -max,
                    max: 0,
                    value: 0
                } ).bind("slide slidechange", function(e, ui) {
                    resList.css("top", ui.value);
					//if( UI.lastSlideChangeTimer != 0 )
					//	clearTimeout(UI.lastSlideChangeTimer);
					
                    //UI.lastSlideChangeTimer = setTimeout( function() {
                    //    resList.animate({
                    //        'top': ui.value
                    //    }, 30 );
                    //}, 50);
                });
            }
        },
        
        /**
         * Window onresize event handler
         */
        onresize: function(suppressSliderReset) {
            var $window = $(window), addWidth/* :number */, addPad/* :number */, favWidth/* :number */, favPad/* :number */;
            if (!UI.smallListMode) {
                $(".results").width($window.width() - $(".controls").width() - 20);
                /** TODO: (addWidth description) */
                addWidth = parseInt($(".video-list .add").width(), 10);
                /** Add padding */
                addPad = parseInt($(".video-list .add").css('padding-left'), 10);
                /** TODO: (favWidth description) */
                favWidth = parseInt($(".video-list .fav").width(), 10);
                /* TODO: favPad description */
                favPad = parseInt($(".video-list .fav").css('padding-left'), 10);
                $(".video-list span").not('.highlight').width(($(".video-list-container").width() / 2) - (addWidth + addPad + favWidth + favPad + 3));
                $(".video-list .add").width(addWidth);
                $(".video-list .fav").width(favWidth);
                $(".video-list .th span").last().width($(".video-list .th span").last().width() + 23);
            }
            else {
                $('.results').width(350);
                $('.video-display').width($window.width() - $(".controls").width() - $(".results").width() - 24);
                $(".fav").next().width(($(".video-list-container").width()) - $(".add").width() - 50);
                $(".video-list .th span").last().prev().width($(".video-list .th span").last().prev().width() + 28);
            }
            
            $(".video-list-container").height($(window).height() - 294);
            $(".video-list-items-container").height($(".video-list-container").height() - $(".video-list-container .th").height());
            
            $("#video-list-slider").height($(".video-list-items-container").height() - 50);
            $(".video-display").height($(".results").height());
            $(".lists").height($window.height() - 307);
            
            $(".trash").width(130);
            
            $(".ad").css("top", $(".lists").height() + $(".navigation").height() - 4 + "px");
            $(".ad").width($(".lists").width() - 16);
            $("#sliderContent").width($(".list").width() - 50);
            if (this.smallListMode) {
                $('.th .fav').css('display', 'none');
                $('.fav').next().next().css('display', 'none'); // this will
                // work better
            }
            if (!suppressSliderReset) {
                UI.configureSlider();
            }
        }
        
    }