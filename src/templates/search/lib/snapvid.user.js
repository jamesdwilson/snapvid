User = {
    authenticated: /* boolean */ initUserAuthenticated, // initial state set in template
    settings:/* object */ initUserSettings,
    login: function(username, password, success, error) {
        $.post("user/login", {
            csrf_token: csrf_token,
            u: username,
            p: password
        }, function(auth) {
            if (auth.username) {
	            _gaq.push(['_trackEvent', "user", "login"]);
                success(auth);
                $("#profile").html('<a href="#" class="userid"/> | <a href="#" id="logoutLink" class="logout">Log out</a>').show();
                $(".userid").append($("<img>").css({
                    'margin-right': '10px',
                    'vertical-align': 'middle'
                }).attr({
                    src: auth.icon,
                    height: 20,
                    width: 20
                })).append(auth.username);
                User.authenticated = true;
                csrf_token = auth.csrf_token;
				delete auth.csrf_token;
				User.settings = auth.settings;
                User.settings['icon'] = auth.icon;
                Playlists.playlists = auth.playlists;
                Playlists.renderPlaylistsNav();
            }
            else 
                if (auth.error) {
                    error(auth);
                }
        }, "json");
    },
    logout: function() {
        // clear playlists
        Playlists.playlists = [];
        Playlists.renderPlaylistsNav();
        User.authenticated = false;
        // TODO: clear search query and results?
        $("#profile").fadeOut("def", function() {
            $("#profile").html('<span class="login"><a href="#" id="loginLink">Log in</a> | <a href="#" class="register" id="registerLink" title="1-Step Register">Register</a></span>').fadeIn();
        });
        $.post("user/logout", {
            csrf_token: csrf_token
        }); // TODO: error handling and
        // notification
    },
    settingsSave: function(settingsToSave, success, error) {
        var sourcelist/* :string */ = "", whiteSpaceTest = new RegExp("\\s"), emailAddress = new RegExp("(\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6})"), settingsUserNameLength = settingsToSave.username.length, emailAddressLength = settingsToSave.email.length, newPassword = settingsToSave.newPassword, newPasswordConfirm = settingsToSave.newPasswordConfirm, newPasswordLength = settingsToSave.newPassword.length, newPasswordConfirmLength = settingsToSave.newPasswordConfirm.length, currentPassword = settingsToSave.currentPassword, currentPasswordLength = settingsToSave.currentPassword.length, currentPasswordVisibility = $('#currentPassword').is(':visible') === true, newAndConfirmPasswordEqual = newPassword === newPasswordConfirm, newPasswordLengthZero = newPasswordLength === 0, newPasswordConfirmLengthZero = newPasswordConfirmLength === 0, currentPasswordLengthZero = currentPasswordLength === 0, settingsToBeSaved;
	    _gaq.push(['_trackEvent', "user", "settingssave"]);
        // themetitle/*:string*/ = $("#themeSwitcher
        // .jquery-ui-themeswitcher-title").text().replace("Theme: ", "");
        // TODO: validate username, all the password fields against empty
        // length and business rules
        // TODO: ensure newPassword == newPasswordConfirm
        if ((whiteSpaceTest.test(settingsToSave.username) === true) || (whiteSpaceTest.test(settingsToSave.email) === true) ||
        (emailAddress.test(settingsToSave.email) === false) ||
        (settingsUserNameLength === 0) ||
        (emailAddressLength === 0)) {
            var invalidUsername = (whiteSpaceTest.test(settingsToSave.username) === true) || (settingsUserNameLength === 0);
            if (invalidUsername) {
                error('invalid username');
            }
            else {
                error('invalid email');
            }
            return;
        }
        User.settings = settingsToSave;
        $("#enginesList").find(".ui-selected").each(function() {
            sourcelist = sourcelist +
            $(this).text().replace(/^\s+|\s+$/g, "") +
            ",";
        });
        if (sourcelist.length > 1) {
            sourcelist = sourcelist.substring(0, sourcelist.length - 1);
        }
        if ((currentPasswordVisibility) && (!(newAndConfirmPasswordEqual) || newPasswordLengthZero || newPasswordConfirmLengthZero || currentPasswordLengthZero)) {
            error('incomplete passwords');
            return;
        }
        settingsToBeSaved = User.settings; //TODO: make less convoluted
        settingsToBeSaved['csrf_token'] = csrf_token;
        $.extend(settingsToBeSaved, settingsToSave);
        $.post("user/savesettings", settingsToBeSaved, function(result) {
            if (result.error) {
                error(result.error);
            }
            else 
                if (result.success) {
	                _gaq.push(['_trackEvent', "user", "savesettings"]);
                    success(result);
                }
        }, "json");
    },
    forgotPassword: function(userEmail, success, error) {
        $.post("user/forgotpassword", {
            csrf_token: csrf_token,
            email: userEmail
        }, function(result) {
            if (result.error) {
                error(result.error);
            }
            else 
                if (result.success) {
	                _gaq.push(['_trackEvent', "user", "forgotpassword"]);
                    success();
                }
        }, "json");
    },
    share: function(userEmail, video, success, error) {
        $.post("user/share", {
            csrf_token: csrf_token,
            title: video.title,
            snapvidId: video.snapvidId,
            shareEmail: userEmail
        }, function(result) {
            if (result.error) {
                error(result.error);
            }
            else 
                if (result.success) {
	                _gaq.push(['_trackSocial', 'email', video.title,
		                location.pathname]);
                    success();
                }
        }, "json");
    },
    register: function(userEmail, success, error) {
        $.post("user/register", {
            csrf_token: csrf_token,
            email: userEmail
        }, function(result) {
            if (result.error) {
                error(result.error);
            }
            else 
                if (result.success) {
	                _gaq.push(['_trackEvent', "user", "registration"]);
                    success();
                }
        }, "json");
    },
}
