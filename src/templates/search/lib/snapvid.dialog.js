Dialog = {
        Share: {
            show: function(video) {
                var Share = function(video) {
                    var userEmail = $("#shareEmail").val();
                    if ($.trim(userEmail).length === 0) {
                        $("#dialog-share span").addClass('ui-state-error-text').text("Aw Snap, let's try that again with an e-mail address.");
                        $("#dialog-share").parent().effect('shake', UI.DIALOG_ERROR_EFFECT_OPTIONS, UI.DIALOG_ERROR_EFFECT_SPEED, function() {
                            $("#ShareEmail").focus();
                        });
                        return;
                    }
                    $("#dialog-share span").removeClass('ui-state-error-text').text("That was a Snap! They will thank you later.");
                    $("#shareEmail").val('');
                    User.share(userEmail, video, function() {
                        setTimeout(function() {
                            $("#dialog-share").dialog("close");
                        }, 1000);
                    }, function(error) {
                        $("#dialog-share span").addClass('ui-state-error-text').text('Aw Snap, ' + error);
                        $("#dialog-share").parent().effect('shake', UI.DIALOG_ERROR_EFFECT_OPTIONS, UI.DIALOG_ERROR_EFFECT_SPEED, function() {
                            $("#ShareEmail").focus();
                        });
                    });
                };
                $('#dialog-share').dialog({
                    autoOpen: true,
                    resizable: false,
                    modal: true,
                    position: 'left',
                    show: 'drop',
                    hide: 'drop',
                    close: function(event, ui) {
                        $("#ShareEmail").val('');
                        $("#dialog-share span").removeClass('ui-state-error-text').html('Share with your friends! (Comma separated)<br/>It\'ll be a Snap.');
                        $(this).dialog('destroy');
                    },
                    buttons: {
                        'Share': function() { 
							Share(video);
												},
                        'Nevermind': function() {
                            $(this).dialog('close');
                        }
                        
                    }
                }).keydown(function(e) {
                    if (e.keyCode === 13) {
                        Share(video);
                    }
                });
                setTimeout(function() {
                    $("#ShareEmail").focus();
                }, UI.ANIMATION_DELAY);
            }
        },
        ForgotPassword: {
            show: function() {
                var forgotPassword = function() {
                    var userEmail = $("#forgotPasswordEmail").val();
                    if ($.trim(userEmail).length === 0) {
                        $("#dialog-forgotpassword span").addClass('ui-state-error-text').text("Aw Snap, let's try that again with an e-mail address.");
                        $("#dialog-forgotpassword").parent().effect('shake', UI.DIALOG_ERROR_EFFECT_OPTIONS, UI.DIALOG_ERROR_EFFECT_SPEED, function() {
                            $("#forgotPasswordEmail").focus();
                        });
                        return;
                    }
                    $("#dialog-forgotpassword span").removeClass('ui-state-error-text').text("That was a Snap! Next: Check your e-mail and reset the password.");
                    $("#forgotPasswordEmail").val('');
                    User.forgotPassword(userEmail, function() {
                        setTimeout(function() {
                            $("#dialog-forgotpassword").dialog("close");
                        }, 1000);
                    }, function(error) {
                        $("#dialog-forgotpassword span").addClass('ui-state-error-text').text('Aw Snap, ' + error);
                        $("#dialog-forgotpassword").parent().effect('shake', UI.DIALOG_ERROR_EFFECT_OPTIONS, UI.DIALOG_ERROR_EFFECT_SPEED, function() {
                            $("#forgotPasswordEmail").focus();
                        });
                    });
                };
                $('#dialog-forgotpassword').dialog({
                    autoOpen: true,
                    resizable: false,
                    modal: true,
                    position: 'left',
                    show: 'drop',
                    hide: 'drop',
                    close: function(event, ui) {
                        $("#forgotPasswordEmail").val('');
                        $("#dialog-forgotpassword span").removeClass('ui-state-error-text').html('Don\'t worry, we\'ll email you a link to reset your password.<br/>It\'ll be a Snap.');
                        $(this).dialog('destroy');
                    },
                    buttons: {
                        'Send me a reset link': forgotPassword,
                        'I remember it': function() {
                            $(this).dialog('close');
                        }
                        
                    }
                }).keydown(function(e) {
                    if (e.keyCode === 13) {
                        forgotPassword();
                    }
                });
                setTimeout(function() {
                    $("#forgotPasswordEmail").focus();
                }, UI.ANIMATION_DELAY);
            }
        },
        Register: {
            show: function() {
                var register = function() {
                    var userEmail = $("#registerEmail").val();
                    if ($.trim(userEmail).length === 0) {
                        $("#dialog-register span").addClass('ui-state-error-text').text("Aw Snap, let's try that again with an e-mail address.");
                        $("#dialog-register").parent().effect('shake', UI.DIALOG_ERROR_EFFECT_OPTIONS, UI.DIALOG_ERROR_EFFECT_SPEED, function() {
                            $("#registerEmail").focus();
                        });
                        return;
                    }
                    $("#dialog-register span").removeClass('ui-state-error-text').text("That was a Snap! Next: Check your e-mail and log in.");
                    User.register(userEmail, function() {
                        setTimeout(function() {
                            $("#dialog-register").dialog("close");
                        }, 1000);
                    }, function(error) {
                        $("#dialog-register span").addClass('ui-state-error-text').text('Aw Snap, ' + error);
                        $("#dialog-register").parent().effect('shake', UI.DIALOG_ERROR_EFFECT_OPTIONS, UI.DIALOG_ERROR_EFFECT_SPEED, function() {
                            $("#registerEmail").focus();
                        });
                    });
                };
                $("#dialog-register").dialog({
                    autoOpen: true,
                    resizable: false,
                    modal: true,
                    position: 'left',
                    show: 'drop',
                    hide: 'drop',
                    close: function(event, ui) {
                        $("#registerEmail").val('');
                        $("#dialog-register span").removeClass('ui-state-error-text').css('font-weight', 'normal').text('Yes. It is that simple.');
                        $(this).dialog('destroy');
                    },
                    buttons: {
                        'Register': register,
                        'Maybe later': function() {
                            $(this).dialog('close');
                        }
                        
                    }
                }).keydown(function(e) {
                    if (e.keyCode === 13) {
                        register();
                    }
                });
                setTimeout(function() {
                    $("#registerEmail").focus();
                }, UI.ANIMATION_DELAY);
            }
            
        },
        Settings: {
            accordionInit: function() {
                $.extend($.ui.accordion.animations, {
                    fastslide: function(options) {
                        $.ui.accordion.animations.slide(options, {
                            duration: (UI.ANIMATION_DELAY / 2) / 2
                        });
                    }
                    
                });
                $('.ui-accordion').accordion({
                    animated: 'fastslide'
                });
            },
            show: function() {
                // When the username is clicked
                // Populate settings dialog:
                Dialog.Settings.populateSettings();
                var settingsSave = function() {
                    var settingsToSave = {
                        adult: $("#allowAdultContent").is(':checked') ? 1 : 0,
                        email: $('#email').val(),
                        first_name: $('#first_name').val(),
                        hd: $("#preferHD").is(':checked') ? 1 : 0,
                        last_name: $('#last_name').val(),
                        username: $('#settings_username').val(),
                        currentPassword: $('#currentPassword').val(),
                        newPassword: $('#newPassword').val(),
                        newPasswordConfirm: $('#newPasswordConfirm').val()
                    };
                    User.settingsSave(settingsToSave, function(result) {
                        $("#dialog-settings span#setChgCnfrm").removeClass('ui-state-error-text').text("Your settings have been saved.");
                        setTimeout(function() {
                            $("#dialog-settings").dialog("close");
                        }, 1000);
                        
                    }, function(error) {
                        $("#dialog-settings").parent().effect('shake', UI.DIALOG_ERROR_EFFECT_OPTIONS, UI.DIALOG_ERROR_EFFECT_SPEED, function() {
                            if (error === 'invalid email') {
                                $("#dialog-settings span#setChgCnfrm").addClass('ui-state-error-text').text('Please enter a valid e-mail address.');
                                $('#emailAddress').focus();
                            }
                            else 
                                if (error === 'invalid username') {
                                    $("#dialog-settings span#setChgCnfrm").addClass('ui-state-error-text').text('Please enter a valid username.');
                                    $('#settings_username').focus();
                                }
                                else 
                                    if (error === 'incomplete passwords') {
                                        $("#dialog-settings span#setChgCnfrm").addClass('ui-state-error-text').text("To change your password: Please enter your current, new, and confirmation password.");
                                        // TODO: error class
                                        $("#dialog-settings").parent().effect('shake', UI.DIALOG_ERROR_EFFECT_OPTIONS, UI.DIALOG_ERROR_EFFECT_SPEED, function() {
                                            $("#currentPassword").focus();
                                        });
                                    }
                                    else {
                                        $("#dialog-settings span#setChgCnfrm").addClass('ui-state-error-text').text("Please make sure your fields are correctly filled out.");
                                    }
                            // TODO: error class
                            $("#settings_username").focus(); // TODO: shake dialog
                        });
                    });
                };
                $('#dialog-settings').dialog({
                    autoOpen: true,
                    resizable: false,
                    modal: true,
                    position: 'left',
                    show: 'drop',
                    hide: 'drop',
                    close: function(event, ui) {
                        $('#dialog-settings input').val('');
                        $('#preferHD,allowAdultContent').removeAttr('checked');
                        $("#dialog-settings span#setChgCnfrm").text("");
                        $(this).dialog('destroy');
                        // TODO: clear selectables/engines list?
                    },
                    buttons: {
                        'Save': settingsSave,
                        Cancel: function() {
                            $(this).dialog('close');
                        }
                    }
                });
            },
            populateSettings: function() {
                $('#first_name').val(User.settings.first_name);
                $('#last_name').val(User.settings.last_name);
                $('#email').val(User.settings.email);
                $('#settings_username').val(User.settings.username);
                $('#settings_gravatar').html($('<img>').attr('src', User.settings.icon));
                if (User.settings.hd) {
                    $('#preferHD').attr('checked', true);
                }
                if (User.settings.adult) {
                    $('#allowAdultContent').attr('checked', true);
                }
            },
        },
        Login: {
            show: function() {
                var login = function() {
                    var username = $.trim($("#username").val()), password = $('#password').val(); //not trimming password as it may have spaces
                    if (username.length === 0 || password.length === 0) {
                        $("#dialog-login span").addClass('ui-state-error-text').text("Aw Snap, let's try that again.");
                        $("#dialog-login").parent().effect('shake', UI.DIALOG_ERROR_EFFECT_OPTIONS, UI.DIALOG_ERROR_EFFECT_SPEED, function() {
                            $("#username").focus();
                        });
                        return;
                    }
                    User.login(username, password, function(auth) {
                        $('#dialog-login').dialog('close'); //TODO slideout? 
                    }, function(auth) {
                        $("#dialog-login span").addClass('ui-state-error-text').text('Aw Snap, ');
                        if (auth.error === "invalid") {
                            $("#dialog-login span").append("invalid username/password, please try again.");
                        }
                        else {
                            $("#dialog-login span").append(auth.error + '.');
                        }
                        $("#dialog-login").parent().effect('shake', UI.DIALOG_ERROR_EFFECT_OPTIONS, UI.DIALOG_ERROR_EFFECT_SPEED, function() {
                            $("#password").val("").focus();
                        });
                    });
                };
                $('#dialog-login').dialog({
                    autoOpen: true,
                    resizable: false,
                    close: function(event, ui) {
                        $('#username,#password').val('');
                        $('#dialog-login span').removeClass('ui-state-error-text').text('');
                        $(this).dialog('destroy');
                    },
                    modal: true,
                    position: 'left',
                    show: 'drop',
                    hide: 'drop',
                    buttons: {
                        'Log in': login,
                        Cancel: function() {
                            $(this).dialog('close');
                        },
                        'Forgot Password': function() {
                            $(this).dialog('close');
                            setTimeout(Dialog.ForgotPassword.show, UI.ANIMATION_DELAY);
                        }
                    }
                }).keydown(function(e) {
                    if (e.keyCode === 13) {
                        login();
                    }
                });
                setTimeout(function() {
                    $('#username').focus();
                }, UI.ANIMATION_DELAY);
            },
        }
    }