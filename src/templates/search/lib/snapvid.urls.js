 URLs = {
 	      /* Valid URL formats:
 	       * http://snapvid.tv/#
 	       * followed by ONE of the following:
 	       * [snapvidvideoid]
 	       * [snapvidvideoid]/[searchterm]
 	       * search/[term]
 	       * user/[username]/[playlistname]
 	       * user/reset/[uniqueid]
 	       */
        parseURL: function() {
            var url, title = "", videoid, query = "", snapvidId, playlistName, parts;
            if (location.hash.length < 2) {
							return;
						}
						parts = location.hash.substring(1).split('/');
            switch (parts[0]) {
	            case "search":
							    log('URL Parser: Searching');
	                query = underscoreUnescape(parts[1]);
	                break;
	            case "user":
	                log('URL Parser: User');
									if (parts[1] === "reset") { //TODO: password reset
	                }
	                else { //playlist
	                    //username = parts[1]; //TODO
											//playlistName = parts[2];//TODO
											if (User.authenticated) { //TODO: remove auth requirement
										  	Playlists.load(Playlists.favoritesID); //make dynamic, load by name
										  }
	                }
	                break;
	            default: //play given snapvid video id, optionally specifying search text
	              log('URL Parser: Playing video specified');
	              snapvidId = parts[0];
	              var siteObj = sites[parseInt(snapvidId[0], 10) - 1];
								if (parts[1]) {
								  query = underscoreUnescape(parts[1]);
								}
								log('playing ' + siteObj.fields.player_type + ' video');
								setTimeout(function() { //settimeout is to defeat some odd race condition of swf holder not being immediately avail
									playVideo({
									  title: query,
									  url: URLs.videoIDToURL(snapvidId),
									  snapvidId: snapvidId,
										externalid: snapvidId.substring(1),
									  playerType: siteObj.fields.player_type,
									  site: siteObj.fields
									});
								}, 10);
                break;
            }
            if (query) {
                $('#searchQuery').val(query);
                Search.start(query);
            }
					},
        videoIDToURL: function(videoID) {
            var localsiteid = parseInt(videoID.slice(0, 1), 10) - 1, // -1 to
            // account
            // for
            // 0-based
            // index
            ext_id = videoID.slice(1), url = 'http://', thumbnail = 'http://', s;
            
            if (typeof sites[localsiteid] !== "undefined") {
                s = sites[localsiteid].fields;
                url += (s.url_prefix ? s.url_prefix : '') + s.url_template.replace(/\{ext_id\}/ig, ext_id) + (s.url_suffix ? s.url_suffix : '');
            }
            else {
                url += videoID.slice(1);
            }
            return url;
        }
    }
