    /**
     * Playlists
     */
    Playlists = {
		favoritesID: 0,
        playlists: initPlaylists,
        // Playlist left nav editing:
        editable_settings: {
            editBy: 'dblclick',
            editClass: 'playlist',
            onSubmit: function(content) {
                var current = $.trim(content.current), previous = $.trim(content.previous), $li = $(this);
                
                if (current === "" && previous === "") {
                    // If user initiates a new playlist, but cancels
                    $li.remove();
                    return;
                }
                if (current === previous) {
                    // No update
                    return;
                }
                if (previous === "") {
                    // New playlist
                    $.post("playlists/add/", {
                        name: current
                    }, function(json) {
                        // TODO: Error handling
                        $li.data("id", json.id);
                    }, "json");
                    return;
                }
                if (current !== previous) {
                    // Update playlist name
                    $.post("playlists/update/", {
                        name: current,
                        id: $li.data("id")
                    }, function(json) {
                        // TODO: Error handling
                        log(json);
                    });
                    return;
                }
            }
        },
        // This is moving the data up and making irrelevant the global variable
        // as set by the server in the tempate.
        // In the future, perhaps dyn gen this js as a template?
        init: function() {
            if (Playlists.playlists.length > 0) {
                $('.fav-btn,#playlist-container li').live('click', function() {
                    Playlists.load($(this).data());
                });
                
            } // out of if to render/reset empty lists
            Playlists.renderPlaylistsNav();
        },
        renderPlaylistsNav: function() {
            var $playlist, i, $tempPlaylistParent = $('<ul>'); // off dom
            if (Playlists.playlists.length === 0) {
                $('.fav-btn').text('Favorites').removeData().append($('<span>').text('0'));
            }
            else {
                for (i = 0; i < Playlists.playlists.length; i += 1) {
                    if (Playlists.playlists[i].title !== "Favorites") {
                        $playlist = $('<li>').addClass('playlist').text(Playlists.playlists[i].title).data(Playlists.playlists[i]);
                        $tempPlaylistParent.append($playlist);
                    }
                    else {
                        $('.fav-btn').text(Playlists.playlists[i].title).data(Playlists.playlists[i]).append($("<span>").text(Playlists.playlists[i].count));
						Playlists.favoritesID = Playlists.playlists[i].id;
                    }
                }
            }
            $('#playlist-container').html($tempPlaylistParent.children());
            // keeps data(), as opposed to grabbing only html()
            $(".playlist").editable(Playlists.editable_settings).keydown(function(e) {
                if (e.keyCode === 13) {
                    $(this).blur();
                }
            });
        },
        createPlaylist: function() {
            $("#playlist-container").prepend($('<li class="playlist"></li>').editable(Playlists.editable_settings).dblclick().keydown(function(e) {
                if (e.keyCode === 13) {
                    $(this).blur();
                }
            }));
            $('input.playlist').focus();
        },
		/**
		 * Adds or removes from favorites
		 * Returns true if now favorited
		 * @param {object}
         *	           video
		 */		
		toggleFavorited: function(video) {
			if (User.authenticated) {
				if (video.playlisted) {
					Playlists.removeFromPlaylist(video);
					return false;
				}
				else {
					video.playlisted = true;
					Playlists.addToPlaylist(Playlists.favoritesID, video);
					return true;
				}
			}
		},
        /**
         * Add video to playlist
         *
         * @param {int}
         *            playlistID
         * @param {object}
         *            video
         */
        addToPlaylist: function(playlistID, video) {
            if (!User.authenticated) {
                throw new Error("User is not authenticated");
            }
	        _gaq.push(['_trackEvent', "playlist", "add"]);
            video['playlistid'] = playlistID;
            $.post("playlists/addItems/", {
                videolist: '[' + JSON.stringify(video) + ']'
            }, function(result) {
                video.playlisted = true;
                video.playlistitemid = result.playlistitemids[0];
                
                $('.fav-btn span').text(parseInt($('.fav-btn span').text(), 10) + 1);
                // TODO: update APPROPRIATE playlist count, this is hardcoded
                // for favorites
            }, "json");
            
        },
        load: function(playlist) {
            var uri = "playlists/getPlaylistItems/" + playlist.id;
            $.ajax({
                url: uri,
                dataType: 'text',
                success: function(resultsDelimited/* :String */) {
                    var results/* :Array.<object> */ = parseDelimitedResults(resultsDelimited);
                    List.render(results);
                    document.title = 'SnapVid.TV - ' + playlist.title;
                    location.href = '#playlist/' + underscoreEscape(playlist.title);
                    $('#status').html($('<b>').text(playlist.title));
                },
                error: function() { // TODO: add global error handling
                }
            });
        },
        /**
         * Removes a video from a playlist
         *
         * @param {int} playlistID
         * @param {object} video
         */
        removeFromPlaylist: function(video) {
            $.post("playlists/deletePlaylistItems/", {
                ids: video.playlistitemid
            }, function(result) {
                video.playlisted = false;
                video.playlistitemid = 0;
            }, "json");
        }
    }