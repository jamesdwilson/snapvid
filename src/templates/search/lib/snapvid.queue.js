    /**
     * Manager for queue on bottom
     */
    Queue = {
        queueVideos:[], // Array of video objects
		queueList: [], // <li> elements
        /**
         * Add video to queue
         *
         * @param {Object}
         *            newVideo to add
         */
        addSingle: function(newVideo/* :Object */) {
            log('Queue.addSingle: ');
            if (!newVideo || typeof newVideo.title === 'undefined') {
                throw new TypeError('Queue.addSingle: Expected: video object');
            }
            Queue._buildQueueItem(newVideo);
			Queue.queueVideos.push(newVideo);
            Queue.render();
            // TODO: get length
            // TODO: this.addAfterIndex(i, newVideo)
        },

        /**
         * Add multiple videos to queue
         *
         * @param {Array}
         *            newVideos to add
         */
        addMultiple: function(newVideos/* :Array<video> */) {
            log('Queue.addMultiple, adding ' + newVideos.length);
            if (newVideos === undefined || !(newVideos instanceof Array)) {
                throw new TypeError('Queue.addMultiple: Expected: array of video objects');
            }
            $.each(newVideos, function(i, newVideo) {
                Queue.queueVideos.push(newVideo);
				Queue._buildQueueItem(newVideo);
            });

            Queue.render();
            // TODO: get length
            // TODO: this.addAfterIndex(i, newVideo)
        },

        /**
         * Build jquery queue object and append to queue list. Private
         * method.
         *
         * @param {Object}
         *            video to add
         */
        _buildQueueItem: function(newVideo) {
            log(newVideo);
            var $newListItem = $('<li>');
            $newListItem.data(newVideo); // transfer video object to new
            // queue item
            $newListItem.html($('<p>').text(newVideo.title));
            $newListItem.attr('title', newVideo.desc);
            $newListItem.prepend(newVideo.thumbnailImgElement.clone(true).attr({
                title: newVideo.desc,
                width: 120,
                height: 70
            }));
            Queue.queueList.push($newListItem);
        },

        render: function() {
            var $videoList = $('#videoList').detach(); // avoid hits to the dom
            $.each(Queue.queueList, function(i, video) {
                $videoList.append(video);
            });
            $('.queue-btn span').text(Queue.queueVideos.length);
            $('#videoListParent').html($videoList);
            UI.onresize();
        },

        /**
         * Add the video to the queue after the specified video object
         *
         * @param {Object} reference video
         * @param {Object} newVideo
         */
        addAfterVideo: function(reference/* :Object */, newVideo/* :Object */) {

        },

        /**
         * @param {Number}
         *            i
         */
        addAfterIndex: function(i/* :Number */, newVideo/* :Object */) {

        },

        /**
         * Remove video by given index
         *
         * @param {Number}  i
         */
        removeByIndex: function(i/* :Number */) {

	        try { // in strict mode delete will throw an error if item does
		        // not exist
		        delete Queue.queueList[i];
		        //TODO: Do we need to clear Queue.queueVideos, too?

	        }
	        catch (e) {

            }
        },

        /**
         * Remove video by video object reference
         *
         * @param {Object} video to remove
         */
        removeByReference: function(video/* :Object */) {

        },
        removeAll: function() {
            Queue.queueList = [];
	        //TODO: Do we need to clear Queue.queueVideos, too?
            $('#videoListParent').html();
        },
	    highlightIndex: function(i/*:Number*/) {
		    var $item = Queue.queueList[i];
		    if(!$item) {
			    throw "Invalid index for highlightIndex";
		    }

		    $('#videoList li.playing,#videoList li .hover').removeClass('playing').find('.hover').remove();
		    $item.addClass('playing');
//		    $item.prepend('<div class="hover"><img src="/static/img/icon_star_lrg.png" width="39" height="37" class="favLrg" /><img src="/static/img/icon_email_lrg.png" width="39" height="37" class="mailLrg" /></div>');
		    $item.find('.hover').addClass('ui-state-active');
	    },
	    playIndex: function(i/*:Number*/) {
			Queue.highlightIndex(i);
		    playVideo($item.data());
	    }
    }