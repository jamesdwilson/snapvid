PlayerInterface = {
        reference: new Player(),
        init: function(video/* :object */) {
            var $playerContent = $('#playerContent'), playerContentSwfId = 'playerContentSwf', swfParams = {
                allowScriptAccess: 'always',
                allowfullscreen: 'true',
                wmode: 'opaque'
            }, swfFlashVars = {}, swfAttributes = {
                id: playerContentSwfId
            };
            
            // TODO: keep open all different types of video players and re-use
            // using note above (if possible)
            function embedSwf(swfUrl) {
                // clear playerContent and create a container for the SWF (will
                // be replaced with <object>
                $playerContent.html('<div id="playerContentSwfHolder">');
                swfobject.embedSWF(swfUrl, 'playerContentSwfHolder', '100%', '100%', '8', null, swfFlashVars, swfParams, swfAttributes);
            }
            
            log('PlayerInterface.init: video type: ' + video.playerType);
            switch (video.playerType) {
                case 'youtube': // TODO: templatize with sites object
                    if (PlayerInterface.reference.playerType !== video.playerType) { // reuse
                        // player
                        // if
                        // possible
                        embedSwf('http://www.youtube.com/v/' + video.externalid + '?fs=1&enablejsapi=1&playerapiid=ytplayer&autoplay=1&rel=0&egm=0&hd=1');
                    }
                    PlayerInterface.reference.init(video.playerType, playerContentSwfId, {
                        id: video.externalid
                    });
                    break;
                case 'moogaloop':
                    $.extend(swfFlashVars, {
                        clip_id: video.externalid,
                        show_portrait: 0,
                        show_byline: 0,
                        show_title: 0,
                        autoplay: 1,
                        js_api: 1,
                        fullscreen: 1,
                        js_onLoad: 'moogaloop_player_loaded',
                    });
                    window['moogaloop_player_loaded'] = function() {
                        PlayerInterface.reference.init(video.playerType, playerContentSwfId);
                    };
                    
                    switch (video.site.domain_name) {
                        case 'vimeo.com':
                            embedSwf('http://vimeo.com/moogaloop.swf');
                            break;
                        case 'collegehumor.com': // api disabled by ch? :(
                            embedSwf('http://www.collegehumor.com/moogaloop/moogaloop.swf');
                            break;
                    }
                    break;
                default: // TODO: pass url, (and video length) make iframe?
                    log('no PlayerInterface handler defined for playerType:' + video.playerType);
                    break;
            }
            
        },
        
        bindEvents: function() {
            $('body').bind({
                'stop.Player': function() {
                    log('bindToPlayerEvents(): stop.Player');
                },
                
                'play.Player': function() {
                    log('bindToPlayerEvents(): play.Player');
                },
                
                'pause.Player': function() {
                    log('bindToPlayerEvents(): pause.Player');
                },
                
                'ready.Player': function() {
                    log('bindToPlayerEvents(): ready.Player');
                },
                'finish.Player': function() {
                    var $playing = $('#videoList li.playing'), nextVideo;
                    log('bindToPlayerEvents(): finish.Player');
                    if (Queue.queueList.length > 0) {
                        $playing.removeClass('playing');
                        nextVideo = $playing.next().addClass('playing').data();
                        playVideo(nextVideo);
                    }
                }
            });
            log('bindToPlayerEvents() - events bound');
        },
        play: function() {
            PlayerInterface.reference.play();
        },
        stop: function() {
            PlayerInterface.reference.stop();
        },
        pause: function() {
            PlayerInterface.reference.pause();
        }
    }
	