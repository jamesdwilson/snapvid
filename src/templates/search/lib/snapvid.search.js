     Search = {
        cachedQuery: undefined,
        cachedResults: undefined,
	     addResultsToQueue:function () {
		     var newVideos/* :Array<video> */ = [], newVideosLIs = $('#resultslist li'), i;
		     log('newVideos.length == ' + newVideosLIs.length);
		     for (i = 0; i <= newVideosLIs.length - 1; i += 1) {
			     newVideos.push($(newVideosLIs[i]).data());
		     }
		     Queue.addMultiple(newVideos);
	     }, renderResults: function(resultsDelimited/* :String */) {
            try {
                var results/* :Array.<object> */ = parseDelimitedResults(resultsDelimited), query = Search.cachedQuery;
                Search.cachedResults = results;
                $('#status').html('Results for &#8220;<b></b>&#8221;:').find('b').text(query);
                $('#searchResultsNav').find('span').text(results.length);
                UI.showBusy(false);
                UI.onresize();
                // TODO: refactor to a namespace, createHistoryItem function
                // or perhaps just navigateTo of some kind, and detect
                // navigations, taking the above actions,
                // this would be more DRY
                document.title = 'SnapVid.TV - Search Results for "' + query + '"';
                location.href = '#search/' + underscoreEscape(query);
	            List.render(results, query);
	            if(Queue.queueVideos.length === 0 && results.length > 0){
		            playVideo(results[0]);
		            Search.addResultsToQueue();
		            Queue.highlightIndex(0);
	            }
            }
            catch (e) {
                $('#status').html("Sorry, an error has occurred. Please try again or contact support@snapvid.tv for help.");
                UI.showBusy(false);
                $("#searchQuery").focus();
            }
        },
        
        start: function(query) {
            if (typeof query === "undefined" || $.trim(query).length === 0 && $.trim($("#searchQuery").val()).length > 0) { // TODO:
                // return most popular results?
                query = $.trim($("#searchQuery").val());// ,
                uri = "search/" + encodeURI(query/* :string */);
            }
            else { // param passed is valid
                query = $.trim(query);
            }
            $('#resultslist').fadeOut(1000);
            $('.controls .ui-selected').removeClass('ui-selected');
            $('#searchResultsNav').addClass('ui-selected', 700);
            if ($.trim(query).length === 0) {
                return;
            }
            
            UI.showBusy(true);
            Search.cachedQuery = query;
            // TODO: refactor UI updates to be more generic and outside of this
            // function
            $('#status').html('Searching for &#8220;<b></b>&#8221;...').find('b').text(query);
            $.ajax({
                url: "search/" + encodeURI(query/* :string */),
                dataType: 'text',
                success: Search.renderResults,
                error: function(xhr, textStatus, errorThrown) {
                    UI.showBusy(false);
                    // TODO: comprehensive error handling, logging, notifying
                    $('#status').html(textStatus + ": " + (errorThrown || ''));
                }
                
            });
        },
        showCached: function() {
            if (Search.cachedQuery && Search.cachedResults) {
                // TODO: refactor UI updates to be more generic and outside of
                // this function
                $('#status').html('Results for &#8220;<b></b>&#8221;:').find('b').text(Search.cachedQuery);
                // TODO: createHistoryItem()
                document.title = 'SnapVid.TV - Search Results for "' + Search.cachedQuery + '"';
                location.href = '#search/' + underscoreEscape(Search.cachedQuery);
                List.render(Search.cachedResults);
            }
        },
        startsByKeyDown: function() {
            $("#searchQuery").hint().focus().keydown(function(e) {
                if (e.keyCode === 13) {
                    Search.start($('#searchQuery').val());
                }
            });
            
        },
        startsByButtonClick: function() {
            $("#searchButton").click(function() {
                Search.start($('#searchQuery').val());
            });
        }
    }