    // Trash will not be available for the 1st release.
    /*
     * function showTrash(show:boolean) { if (show) {
     * $(".list").width($(window).width() - 150); $('.trash').show('slow'); }
     * else { $('.trash').hide(); } }
     */
    /**
     * Onload
     */
    $(function() { // Begin onload:
        $(window).resize(UI.onresize);
        UI.onresize();
        var $slider = $("#video-list-slider");
        $('.video-list-items').live('mousewheel', function(e, delta) {
            if ($slider.is(':visible')) {
                $slider.slider('value', $slider.slider('value') + (delta * 30));
            }
        });
        // Search area
        Search.startsByKeyDown();
        Search.startsByButtonClick();
        
        // Profile/login area
        $('#loginLink').live('click', Dialog.Login.show);
        // Playlists
        $("#add-playlist").click(function() {
            Playlists.createPlaylist();
        });
        
        // Settings
        $("#profile .userid").live('click', Dialog.Settings.show);
        Dialog.Settings.accordionInit();
        
        // Register dialog
        $("#registerLink").live('click', Dialog.Register.show);
        
        // Logout
        $("#logoutLink").live('click', User.logout);
        
        // Video list add all header button:
        $('#video-list-addAll').click(function() {
            var newVideos/* :Array<video> */ = [], newVideosLIs = $('#resultslist li'), i;
            log('newVideos.length == ' + newVideosLIs.length);
			for (i = 0; i <= newVideosLIs.length - 1; i += 1) {
                newVideos.push($(newVideosLIs[i]).data());
            }
            Queue.addMultiple(newVideos);
        }).find('img').hover(function() {
            $(this).attr('src', '/static/img/icon_add_hover.png');
        }, function() {
            $(this).attr('src', '/static/img/icon_add.png');
        });
        
        // Queue init
        $('#videoList li:not(.playing)').live('click', function() {
            var $this = $(this);
            log('Queue init: #videolist.li live click');
            // Highlight playing video in slider - TODO: refactor to UI
            $('#videoList li.playing,#videoList li .hover').removeClass('playing').find('.hover').remove();
            $this.addClass('playing');
			$this.prepend('<div class="hover"><img src="/static/img/icon_star_lrg.png" width="39" height="37" class="favLrg" /><img src="/static/img/icon_email_lrg.png" width="39" height="37" class="mailLrg" /></div>');
			$(this).find('.hover').addClass('ui-state-active');

            playVideo($this.data(), this);
        });
        $('.hover .favLrg').live('click', function(){
			if (User.authenticated) {
				var v = $(this).parents('li').data(), added = Playlists.toggleFavorited(v);
				//TODO: toaster or UI notification
			}
			else {
				Dialog.Login.show();
			}
		});
	 	
		$('.hover .mailLrg').live('click', function(){
            if (User.authenticated) {
                var v = $(this).parents('li').data();
                Dialog.Share.show(v);
            }
            else {
                Dialog.Login.show();
            }
		});
		
        $("#searchResultsNav").click(Search.showCached);
        $(".queue-btn").click(function() {
            document.title = 'SnapVid.TV - Queue';
            location.href = '#queue';
			$('#status').html($('<b>').text('Queue'));
            List.render(Queue.queueVideos);
        });
        
        // Search
        $("#enginesList").selectable();

	    var foundUrl = URLs.parseURL();
        Playlists.init();
        PlayerInterface.bindEvents();
        $(window).load(UI.onload);
	    loadYTFeedScript();
	    var ytFeedRetryDelay = 100;

	    var handleYTFeed = function () {
		    if(ytFeedVideos.length == 0 && ytFeedRetryDelay < 5000) {
			    ytFeedRetryDelay+=10;
			    setTimeout(handleYTFeed, ytFeedRetryDelay);
			    return;
		    }
//		    if(!foundUrl) {
		    playVideo(ytFeedVideos[0]);
		    List.render(ytFeedVideos);
		    Search.addResultsToQueue();
		    Queue.highlightIndex(0);
//		    }
	    };
		handleYTFeed();
	    // showTrash(false);
        // end onload
    });