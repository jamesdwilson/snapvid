 List = {
        /**
         * Render an array of video objects, optionally specifying a query to
         * enhance result listings and metadata
         */
        render: function(videoList/* :Array.<object> */, query/* : string */) {
            var ondblclickevent = function(event) {
                log('search.ondoubleclickevent');
            }, onclickevent = function(event) {
                log('search.onclickevent');
                playVideo($(this).parent().data(), $(this).parent());
            }, altRow = true, altClass = 'alt', $resultslist = $('<ul class="video-list-items" id="resultslist">'), $img;
            
            if (typeof $itemTemplate === "undefined") {
                $itemTemplate = $('#resultslistItemTemplate').clone().removeAttr('id'); // cloning
                // prevents hit to dom each iteration
            }
            
            $.each(videoList, function(i, video) { // render the results
                var $newListItem = $itemTemplate.clone(), $item = $newListItem.disableTextSelect();
                $item.find('.video-title').text(video.title); // TODO:.highlight(query);
                $item.find('.video-desc').text(video.desc); // .highlight(query);
                // mark it as "starred" if it is their playlists somewhere
                if (video.playlisted) {
                    $item.find('.fav img').attr('src', '/static/img/icon_star.png');
                }
                $img = $('<img>').attr('src', video.thumbnail);
                video['thumbnailImgElement'] = $img;
                $newListItem.attr('title', video.desc).data(video).appendTo($resultslist);
                if (altRow) {
                    $newListItem.addClass(altClass);
                }
                altRow = !altRow;
            });
            $('.video-list-items-container').html($resultslist);
            $('#resultslist li .video-title,#resultslist li .video-desc').dblclick(ondblclickevent).click(onclickevent);
            // Fav
            $('.page .video-list .fav img').hover(function() {
                $(this).attr('src', '/static/img/icon_star_hover.png');
            }, function() {
                var $this = $(this), v = $this.parents('li').data();
                log('v.playlisted ' + v.playlisted);
                log(v);
                if (!v.playlisted) {
                    $this.attr('src', '/static/img/icon_star_gray.png');
                }
            }).click(function() {
                var $this = $(this), v = $this.parents('li').data();
                if (!User.authenticated) { // TODO: refactor to jqueryui dialog
                    // $('#dialog-generic').attr('title',
                    // 'Error').find('p').text('You must be logged in to add or
                    // remove from playlists.').dialog({
                    // hide: 'drop',
                    // show: 'drop',
                    // close: function() {
                    // $(this).dialog('destroy');
                    // },
                    // buttons:{
                    // 'Login now': function () {
                    // $(this).dialog('close');
                    // $('#loginLink').click();
                    // },
                    // OK:function () {
                    // $(this).dialog('close');
                    // }
                    // }
                    // }).dialog('open');
                    return;
                }
                
                if (Playlists.toggleFavorited(v)) {
                    $this.attr('src', '/static/img/icon_star.png');
                }
                else {
                    $this.attr('src', '/static/img/icon_star_gray.png');
                    $('.fav-btn span').text(parseInt($('.fav-btn span').text(), 10) - 1);
                }
            });
            // Add
            $('.video-list-items-container').find('.add img').hover(function() {
                $(this).attr('src', '/static/img/icon_add_hover.png');
            }, function() {
                $(this).attr('src', '/static/img/icon_add.png');
            }).click(function() {
                Queue.addSingle($(this).parents('li').data());
            });
            
            UI.onresize();
            $('#resultslist').fadeIn(250);
        }
    }