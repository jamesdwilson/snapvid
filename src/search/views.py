"""Views page for Search App"""
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.utils import simplejson
import engines
from django.views.decorators.cache import cache_page
from playlists.views import getPlaylistsByUser, getPlaylistcount, getPlaylistItemId
from django.core import serializers
import re
import urlparse
import logging
import string
import random
from models import Site
from django.contrib.auth.models import AnonymousUser
from users.models import User
from users.views import get_settings_by_user
from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from django.template.context import RequestContext

LINE_DELIMITER = '\r'
DELIMITER = '|'


def serializeVideo(user, video):
    videoeo_serialized = ''
    domain_name = video.externalID.split('_')[0]
    site = None
    playlisted = '0'
    playlistitemid = '0'
#    if user and (type(user) is not AnonymousUser) and playlistitemid:
#        playlistitemid = getPlaylistItemId(user, video) or '0'
#        playlistitemid = str(playlistitemid)
    try:
        site = Site.objects.filter(domain_name=domain_name).get()
    except:
        pass
    localsiteid = '0'
    externalid = ''
    thumbnail = ''
    if site is not None:
        localsiteid = site.id
        externalid = video.externalID[len(domain_name) + 1:]
        if site.thumbnail_template is not None and len(site.thumbnail_template) > 0:
            thumbnail = ''
        else:
            thumbnail = short_url(video.thumbnail)    
    else:
        externalid = short_url(video.url)
        thumbnail = short_url(video.thumbnail)
    return DELIMITER.join([re.sub('[\||\n|\r]', '-', video.title.strip(string.whitespace + '-.:')), #Jersey Face| 
                                         re.sub('[\|\n|\r]', '-', video.desc.strip(string.whitespace + '-.:')), #Rasheed Wallace giving an assistant coach a souvenir. Right in the mouth.
                                         str(localsiteid) + str(externalid), #31761766|
                                         thumbnail, #2.media.collegehumor.cvcdn.com/5/4/collegehumor.d5120b35ee04c4c9d6ea2a64fa14c95e
                                         playlistitemid #if the user has this video in their playlists
                                         ]).strip(DELIMITER)
def serializeVideos(user, videos):
    videos_serialized = []
    for video in videos:
        videos_serialized.append(serializeVideo(user, video))
    return LINE_DELIMITER.join(videos_serialized)

def index(request):
    """Default web root 
    """
    sites_list_json = serializers.serialize('json', Site.objects.filter(enabled=True)) 
    playlists_json = "[]"
    
    user_count = User.objects.count()
    pagevars = {"favorites_count":"0", "sites_list_json": sites_list_json, "playlists_json": playlists_json, "user_count":user_count, "user_authenticated": 'false', 'settings_json':{}}
    if request.user.is_authenticated():
        playlists = getPlaylistsByUser(request.user)
        playlists_json =  simplejson.dumps(playlists)
        settings_json = simplejson.dumps(get_settings_by_user(request.user))
        pagevars = {"favorites_count": getPlaylistcount(request.user, "Favorites"),
                    "engines":engines.engines_friendly,
                    "user":request.user,
                    "playlists": playlists,
                    "sites_list_json": sites_list_json, 
                    "playlists_json": playlists_json,
                    "user_count" :user_count,
                    "user_authenticated": 'true',
                    'settings_json': settings_json}
    return render_to_response("search/index.html", pagevars)

def short_url(url):
    if url[:7] == "http://":
        url = url[7:]
    if url[:4] == "www.":
        url = url[4:]
    return url

@cache_page(60 * 60) #TODO: Caching for an hour
def query(request, params):
    if len(params) == 0:
        return
    serialized_results = serializeVideos(request.user, engines.query(params))
    return HttpResponse(serialized_results)

def snapvid_js(request):
    #TODO: integrate pagevars from index, remove from global scope entirely
    csrf_token = ''.join(random.choice(string.letters + string.digits) for i in xrange(25))
    request.session['csrf_token'] = csrf_token
    return render_to_response("search/lib/snapvid.js", {'csrf_token':csrf_token})