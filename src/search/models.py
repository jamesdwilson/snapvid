from django.db import models
from playlists.views import getPlaylistItemId
from django.contrib.auth.models import AnonymousUser

class Site (models.Model):
    domain_name = models.CharField(max_length=50)
    player_type = models.CharField(max_length=50)
    friendly_name = models.CharField(max_length=100)
    url_template = models.CharField(max_length=300)
    url_prefix = models.CharField(max_length=100)
    url_suffix = models.CharField(max_length=100)
    thumbnail_template = models.CharField(max_length=300)
    thumbnail_prefix = models.CharField(max_length=100)
    enabled = models.BooleanField(default=True)

# Create your models here.
class Video(models.Model):
    def __unicode__(self):
        return self.title + '[' + self.url + ']'

    """Generic video object
    
    Testing for duplicates:
    >>> v = Video()
    >>> v.title = 'awesomest video evar'
    >>> v.url = 'http://example.com'
    >>> v.externalID = 'example.com_123'
    >>> v.save()
    >>> v2 = Video()
    >>> v2.title = 'awesomest video evar'
    >>> v2.url = 'http://example.com'
    >>> v2.externalID = 'example.com_123'
    >>> v2.save() #error should be raised here
    Traceback (most recent call last):
         ...
    IntegrityError: column externalID is not unique
    """
    title = models.CharField(max_length=100)
    desc = models.CharField(max_length=200, null=True, blank=True)
    cat = models.CharField(max_length=50, null=True, blank=True)
    tags = models.CharField(max_length=50, null=True, blank=True)
    pageUrl = models.CharField(max_length=300, null=True, blank=True)
    url = models.CharField(max_length=300)
    duration = models.IntegerField(default=0, null=True)
    rating = models.CharField(max_length=10, null=True, blank=True)
    thumbnail = models.CharField(max_length=300 , null=True, blank=True)
    site = models.ForeignKey(Site)
    externalID = models.CharField(unique=True, max_length=100 , null=True, blank=True) #external sites' ID for the video
#    def serialize(self):
#        return serializeVideo(user=None, Video(self))
        
class SearchResultVideo(Video):
    def __unicode__(self):
        return self.title + '[' + self.url + ']'
    rank = models.IntegerField()
    title_occurrences = models.IntegerField()
    desc_occurrences = models.IntegerField()   

