import itertools
import youtube, vimeo #,internal #,truveo
import random
import logging
import re
import traceback
from multiprocessing import Process, Lock, Pool
import types
from sortvideos import sort_results

#all_engines = [ vimeo, youtube, internal ] 
all_engines = [ vimeo, youtube ]
#Order based on estimated response time, longest first

#TODO: document or use proper __str__ (i think i tried to use __str__ before and was unsuccessful, hence this workaround)
engines_friendly = [ engine.friendly_name for engine in all_engines ]

active_query = ""
map_final_results = []

def run_engine(n):
    try: #if one engine fails, continue on with the next engine
        engine = all_engines[n]
        logging.info("SEARCH ENGINE: Starting ASYNC call to " + engine.friendly_name)    
        return engine.query( active_query )
    except:
        logging.error("SEARCH ENGINE: failed for " + engine.friendly_name + ", error: " + traceback.format_exc())
        pass #keep on trucking

#pool map_async callback
def engines_complete(map_results):
    global map_final_results
    map_final_results = []
    for m in map_results:
        map_final_results.append(list(m))

def query(query, engines_list=all_engines):
    global active_query
    active_query = query
    query = re.sub('[^-+"a-z0-9_ ]', '', query.lower()).strip() 
    #remove anything that is not alphanumeric or a space from the string, allowing for +,-," for advanced search 
    if len(query) == 0:
        return []
    logging.info("SEARCH ENGINE: Query: '" + query + "'")
    #TODO: Save queries to db
    
    unsorted_result_count = 0
    
    p = Pool()
    final_results = []
    map_object = p.map_async(run_engine, range(len(all_engines)), None, engines_complete)
    map_object.wait(5000) #5 sec
    #supposedly the max number of seconds to wait for the async calls to complete..maybe we're blocking too much for this to matter,
    #but this does not seem to act as intended. anything 5 or above seems to work identically     
    for result in map_final_results:
        unsorted_result_count = unsorted_result_count + len(result)
        final_results.extend(result)

    sorted_results = sort_results(query, final_results)
    if(unsorted_result_count != len(sorted_results)):
        logging.error('SEARCH ENGINE: non-fatal while querying ' + query + ', sorted and unsorted result counts are different')
    return sorted_results
