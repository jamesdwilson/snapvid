from search.models import SearchResultVideo
import logging
import simplejson
import urllib2
import traceback
import re

#TODO: Work with Truveo rating system, if it's worth the time.
#TODO: If possible find a way to get video duration. There is no way to get this from json response fields.
#TODO: Refine request parameters
#TODO: Refine response fields

friendly_name = "Truveo"

def query(params):
    logging.info("TRUVEO SEARCH ENGINE: looking for '" + params + "'")
    results = []
    try:
        truveo_base_query = 'http://xml.truveo.com/apiv3?appid=070327263ae172900&method=truveo.videos.getVideos&results=50&start=0&showAdult=0&format=json&query='
        query_url = truveo_base_query + urllib2.quote(params)
        logging.debug("TRUVEO SEARCH ENGINE - Loading URL: " + query_url)
        json_results = simplejson.load(urllib2.urlopen(query_url))
        rank = 0
        for vid in json_results["response"]["data"]["results"]["videoSet"]["videos"]:
            try:
                rank += 1
                video_id = str(vid["id"])
                title = vid["title"]
                upload_date = vid["dateFound"] or ""
                duration = ""
                thumbnail = vid["thumbnailURL"] or ""
                desc = vid["description"] or ""
                if "videoPlayerEmbedTag" in vid:
                    src_value = re.search('src="(.*?)"', vid["videoPlayerEmbedTag"])
                    embed_url = src_value.group(1)
                    if embed_url is not None:
#                        logging.debug('TRUVEO SEARCH ENGINE - Adding video: rank = ' + str(rank) + ', title = ' + title + ','                                        
#                                      'url = ' + embed_url + ', thumbnail = ' + thumbnail + ',' 
#                                        + 'externalID = ' + video_id + ', desc = ' + desc)
                        results.append(SearchResultVideo(rank = str(rank), title = title,
                                        url = embed_url, thumbnail = thumbnail, 
                                        externalID = video_id, desc = desc))
            except:
                logging.error("TRUVEO SEARCH ENGINE: Could not parse video: '" + str(vid) + "', traceback: " + traceback.format_exc())
                pass
    
    except:
        logging.error("TRUVEO SEARCH ENGINE: TOTAL FAILURE FOR QUERY: '" + params + "', traceback: " + traceback.format_exc())
    logging.info("TRUVEO SEARCH ENGINE: RESULT COUNT: " + str(len(results)))
    return results