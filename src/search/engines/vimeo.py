'''
Created on Jan 5, 2010

@author: james
'''
from django.conf import settings
from vimeolib import VimeoClient
import logging
import simplejson
import urllib2
from search.models import SearchResultVideo
import traceback

friendly_name = "Vimeo"
#
def query(params):
    logging.info("VIMEO SEARCH ENGINE: looking for '" + params + "'")
#    json_results = simplejson.load(urllib2.urlopen('http://vimeo.com/api/rest/v2?format=json&method=vimeo.videos.search&full_response=1&per_page=50&sort=relevant&query=' + urllib2.quote(params)))
    results = []
    try:    
        v = VimeoClient(key=settings.VIMEO_API_KEY,
                        secret=settings.VIMEO_API_SECRET, format="JSON",
                        cache_timeout=0)
        json_results = v.vimeo_videos_search(query = params, full_response = 1, sort = 'relevant')['video']
        rank = 0
        for vid in json_results:
            try:
                rank += 1
                video_id = 'vimeo.com_' + vid['id']
                title = vid["title"]
                upload_date = vid["upload_date"] or ""
                desc = vid["description"] or ""
                duration = vid["duration"] or ""
                thumbnail = ""
                
                #TODO: probably needs fine tuning
                try:
                    rating = ((int(vid["number_of_likes"]) * 150) /
                              int(vid["number_of_plays"]))
                except:
                    rating = 5 #TODO: hack
                    
                pageUrl = vid["urls"]["url"][0]["_content"]
        
                try:
                    if vid["thumbnails"] is not None:
                        thumbnail = vid["thumbnails"]["thumbnail"][0]["_content"]
                except:
                    thumbnail = ""
                    
                #TODO: refactor to use video-api only?
                swf_url = ("http://vimeo.com/moogaloop.swf?clip_id=" + video_id +
                "&server=vimeo.com&show_title=0show_byline=0&show_portrait=0" + 
                "&color=18272&fullscreen=1&autoplay=1")
                
                results.append(SearchResultVideo(rank = rank, title = title,
                                desc = desc, url = swf_url,
                                pageUrl = pageUrl, duration = duration,
                                thumbnail = thumbnail, rating = rating, externalID = video_id))
            except:
                logging.error("VIMEO SEARCH ENGINE: Could not parse video: " + str(vid) + "', traceback: " + traceback.format_exc())
                pass #continue to next video
    except:
        logging.error("VIMEO SEARCH ENGINE: TOTAL FAILURE FOR QUERY: '" + params + "', traceback: " + traceback.format_exc())
    logging.info("VIMEO SEARCH ENGINE: RESULT COUNT: " + str(len(results)))
    return results
