import logging
import traceback
last_video = None #used for debugging

def sort_this(v):
    global last_video
    last_video = v
    return ((v.rank or 10), (v.rating or 5) * -1, (v.title_occurrences or 0) *
             -1, (v.desc_occurrences or 0) * -1)

def sort_results(query, results):
    global last_video
    try: #attempt to sort the video results, if an uncatchable exception
        #occurs, return unsorted list
        for video in results:
            last_video = video
            try: #try iterating through each video to sort it, if this video
                # fails, continue to next one
                title_occurrences = 0
                desc_occurrences = 0
                for word in query.split():
                    try:
                        title_occurrences += video.title.lower().count(word)
                    except UnicodeDecodeError:
                        pass
                    try:
                        desc_occurrences += video.desc.lower().count(word)
                    except AttributeError:
                        logging.error("sort_results Failed to load desc for "
                                       + str(video))
                    except UnicodeDecodeError:
                        pass

                video.title_occurrences = title_occurrences
                video.desc_occurrences = desc_occurrences
            except:
                video.title_occurrences = 0
                video.desc_occurrences = 0
                logging.error("sort_results failed on video: " +
                               traceback.format_exc())
                try:
                    logging.error("sort_results video failure title:" +
                                   video.title)
                except:
                    pass

                pass
            results.sort(key=sort_this)
        for v in results:
            del v.title_occurrences
            del v.desc_occurrences
    except:
        logging.error("sort_results COMPLETELY failed with query: " + query +
                       ', error: ' + traceback.format_exc())
        logging.error("sort_results COMPLETELY failed with last video: " +
                       str(last_video))
        pass
    #return sorted list if successful, unsorted original list if error
    return results 