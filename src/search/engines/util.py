"""Utility functions"""

def truncate( text, length, ellipsis = '...' ):
    """
    Nicely truncate text
    Intelligent truncation of text means that truncation does not take place
    inside a word but only between words. So the required length is only an
    approximation. The result text might be a bit longer than the required
    length.
    """
    if text is None:
        text = ''
    if not isinstance( text, basestring ):
        raise ValueError( "%r is no instance of basestring or None" % text )

    # thread other whitespaces as word break
    content = ( text.replace( '\r', ' ' ).replace( '\n', ' ' ).
                replace( '\t', ' ' ) )
    # make sure to have at least one space for finding spaces later on
    content += ' '

    if len( content ) > length:
        # find the next space after max_len chars (do not break inside a word)
        pos = length + content[length:].find( ' ' )
        if pos != ( len( content ) - 1 ):
            # if the found whitespace is not the last one add an ellipsis
            text = text[:pos].strip() + ' ' + ellipsis

    return text
