""" @author: James Wilson """

from django.utils import simplejson 
from search.models import SearchResultVideo
import util
import logging
import simplejson
import urllib2
import traceback

url_pattern = r"youtube.com/watch\?.*?v=.{11}"

friendly_name = "YouTube"

def query(search_terms):
    """Executes query against YouTube"""
    logging.info("YOUTUBE SEARCH ENGINE: looking for '" + search_terms + "'")
    results = []
# TODO: re-integrate these options in json-c call
#    youtube_query.orderby = 'relevance'
#    youtube_query.racy = 'include'
#    youtube_query.max_results = 50
    try:
        youtube_query = 'http://gdata.youtube.com/feeds/api/videos?v=2&alt=jsonc&q='
        json_results = simplejson.load(urllib2.urlopen(youtube_query + urllib2.quote(search_terms)))
        rank = 0
        for video in json_results['data']['items']:
            try:
                rank += 1
                vid = get_video_from_feed(video)
                vid.rank = rank
                if vid:
                    results.append(vid)
            except:
                #TODO: Log failure 
                pass
    except:
        logging.error("YOUTUBE SEARCH ENGINE: TOTAL FAILURE FOR QUERY: '" + search_terms + "', traceback: " + traceback.format_exc())
        pass
    logging.info("YOUTUBE SEARCH ENGINE: RESULT COUNT: " + str(len(results)))
    return results

def get_video_from_feed(entry):
    """Converts a YouTube API Feed into a Video object"""
    vid = SearchResultVideo()
    vid.title = entry['title']
    vid.externalID = 'youtube.com_' + entry['id']
    try:
        vid.pub = entry['uploaded']
    except:
        pass
    
    try:
        vid.desc = util.truncate(entry['description'], 100, "").rstrip()
    except:
        vid.desc = ''
        pass
    
    try:
        vid.cat = entry['category']
    except:
        vid.cat = ''
        pass
    
    try:
        if(entry['tags'] is not None):
            vid.tags = ' '.join(entry['tags'])
    except:
            vid.tags = ''
            pass
    vid.pageUrl = entry['player']['default']
    try:
        vid.url = entry['content']['5']
    except:
        #Alternate formats are available but no flash video is available for this video
        return None
    
    try:
        vid.duration = entry['duration']
    except:
        pass

    # non entry.media attributes
#    if entry.geo is not None:
#        vid.geo = str(entry.geo.location())
#
#    if entry.statistics is not None and entry.statistics.view_count is not None:
#        vid.viewCount = entry.statistics.view_count

#    if entry['rating'] is not None:
#        vid.rating = int(entry['rating']).average[:1] * 2 #10-based ratings
#    else:
    vid.rating = 5 #TODO: fix ratings
    try:
        vid.thumbnail = entry['thumbnail']['sqDefault'] #120 x 90 - Prefer standard quality (lower res) for thumbnails for fast display
    except:
        try:
            vid.thumbnail = entry['thumbnail']['hqDefault'] # 480 x 436 - too big for most of our uses
        except:
            vid.thumbnail = ''    
    return vid

#TODO: Make use of these?
#    result.append( {"title":v.title, "pub":v.pub, "desc":v.desc, 
#    "cat":v.cat, "tags":v.tags, "pageUrl":v.pageUrl, "url": v.url,
# "duration":v.duration, "rating":v.rating, "thumbnail":v.thumbnail} )
