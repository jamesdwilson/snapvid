'''
Search engine for internal, scraped videos
Created on Jan 5, 2010

@author: james
'''
import logging
from search.models import Video, SearchResultVideo
from django.db.models import Q

friendly_name = "Others"
#REQUIRED for fulltext searching: ALTER TABLE search_video ADD FULLTEXT(title(3), `desc`(3));
def query(query):
    logging.info("INTERNAL SEARCH ENGINE: looking for '" + query + "'")

    matches = Video.objects.filter(Q(title__search=query) | Q(desc__search=query))[:50]

    rank = 0
    results = []
    for v in matches: 
        rank += 1
        rating = 5 #TODO: this ranking and rating is a hack
        results.append(SearchResultVideo(rank=rank, title=v.title, desc=v.desc,
                        url=v.url, pageUrl=v.pageUrl, rating=rating, thumbnail=v.thumbnail, externalID=v.externalID))

    logging.info("INTERNAL SEARCH ENGINE: RESULT COUNT: " + str(len(results)))
    return results
