""" Django settings for snapvid project."""
import logging
logging.basicConfig(level=logging.DEBUG, filename='debug.log',
                    format='%(asctime)s %(levelname)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

VIMEO_API_KEY = 'b704e560f151405a2dc5b7584806f5b2'
VIMEO_API_SECRET = 'd8ce4c7d8600d7d9'

GRAVATAR_DEFAULT_IMAGE = 'wavatar'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (('James Wilson', 'james@jameswilson.name'), ('Cory Dambach', 'corydambach@gmail.com'))
 
MANAGERS = ADMINS

CACHE_BACKEND = 'memcached://127.0.0.1:11211/'

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
#        'NAME': 'vse.sqlite3', # Or path to database file if using sqlite3.
#        'USER': '', # Not used with sqlite3.
#        'PASSWORD': '', # Not used with sqlite3.
#        'HOST': '', # Set to empty string for localhost. Not used with sqlite3.
#        'PORT': '', # Set to empty string for default. Not used with sqlite3.
#    }
#}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'vse', # Or path to database file if using sqlite3.
        'USER': 'root', # Not used with sqlite3.
        'PASSWORD': '', # Not used with sqlite3.
        'HOST': '', # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '', # Set to empty string for default. Not used with sqlite3.
    }
}

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'noreply@snapvid.tv'
EMAIL_HOST_PASSWORD = '6T>|*:7|RG4I|a'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = ''

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
#TODO: Make new secret key before launch!
SECRET_KEY = '1tk)fqv^@m0_4dk#n+797c-k)p-j3x*t%&rpxzyqot)!#w(&x1'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
 )

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    
 )

ROOT_URLCONF = 'src.urls'

TEMPLATE_DIRS = (
                 'templates',
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
 )

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'search',
    'users',
    'playlists',
    'django.contrib.admin',
    'gravatar',
 )


AUTHENTICATION_BACKENDS = (
    'users.backends.EmailOrUsernameModelBackend',
     'django.contrib.auth.backends.ModelBackend',
 )

AUTH_PROFILE_MODULE = 'users.UserProfile'
